//#include <ros/ros.h>
//#include <unistd.h>

//#include <fstream>
//#include <sys/types.h>
//#include <stdlib.h>
//#include <sstream>
//#include <string>
//#include <iomanip>

//#include "sensor_msgs/Imu.h"
//#include "sensor_msgs/JointState.h"
//#include "sensor_msgs/LaserScan.h"
//#include "sensor_msgs/PointCloud.h"
//#include "std_msgs/Float32.h"
//#include "std_msgs/String.h"
//#include "sensor_msgs/ChannelFloat32.h"
//#include "nav_msgs/OccupancyGrid.h"

#include <cuda.h>
#include <cuda_runtime.h>

#define THREADS_PER_BLOCK 1024
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3
#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925

//Функция для оценки проходимости точки
__global__ void computePassability(float *dev_p, int *dev_p_size, bool *dev_pass, int *dev_packNmb, float *dev_angleTresh) {

    //Вычисляем номер нити
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    while(tid < *dev_packNmb) {
                //Для всех точек линии вычисляем угол
                for(int j = 0; j < LASER_NMB; j++) {

                      //Изначально устанавливаем точку непроходимой
                      dev_pass[tid * LASER_NMB + j] = false;

                    //Смотрим, не отфильтрована ли текущая точка
                    if (dev_p[tid * LASER_NMB * POINTS_DIM + (j)*POINTS_DIM + 0] != 0 && dev_p[tid * LASER_NMB * POINTS_DIM + (j)*POINTS_DIM + 1] != 0 &&
                            dev_p[tid * LASER_NMB * POINTS_DIM + (j)*POINTS_DIM + 2] != 0  )
                    {

                        //Координаты точки
                        float x_p, y_p, z_p, x, y, z;
                        //Определяем предыдущую точку
                        int prev_point = j-1;

                        //В случае, если предыдущая точка отфильтрована, то ищем предыдущую дальше, пока не наткнёмся на неотфильтрованную
                        while((dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)*POINTS_DIM + 0] == 0 &&
                              dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)*POINTS_DIM + 1] == 0 &&
                              dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)*POINTS_DIM + 2] == 0)
                              && prev_point >= -1)
                        {

                            //Уменьшаем на один значение
                            prev_point--;
                        }

                        //Текущая точка
                        int current_point = j;

                        while((dev_p[tid * LASER_NMB * POINTS_DIM + (current_point)*POINTS_DIM + 0] == 0 &&
                               dev_p[tid * LASER_NMB * POINTS_DIM + (current_point)*POINTS_DIM + 1] == 0 &&
                               dev_p[tid * LASER_NMB * POINTS_DIM + (current_point)*POINTS_DIM + 2] == 0)
                               && current_point <= LASER_NMB)
                        {
                           //Увеличиваем на одно значени
                           current_point++;
                        }

                        //Если значение prev_point равно -1, то это означает, что точка не найдена
                        if(prev_point > -1 && current_point <LASER_NMB) {
                            x_p = dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)* POINTS_DIM + 0];
                            y_p = dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)* POINTS_DIM + 1];
                            z_p = dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point)* POINTS_DIM + 2];
                            x = dev_p[tid * LASER_NMB * POINTS_DIM + current_point* POINTS_DIM + 0];
                            y = dev_p[tid * LASER_NMB * POINTS_DIM + current_point* POINTS_DIM + 1];
                            z = dev_p[tid * LASER_NMB * POINTS_DIM + current_point* POINTS_DIM + 2];

                            //Вычисляем расстояние
                            float xyDistance = sqrt((x - x_p)*(x - x_p) + (y - y_p)*(y - y_p));
                            float zDistance = z - z_p;

                            //Смотрим угол
                            float tanAlpha = zDistance / xyDistance;
                            //Если угол меньше, то отмечаем точку как проходимую
                            if ((std::abs(tanAlpha) < std::abs(tan(*dev_angleTresh)) && z < 0.5 && z_p < 0.5) || (z > 2 && z_p > 2)) {

                                 dev_pass[tid * LASER_NMB + current_point] = true;
                                 //Если при этом точка находится в начале, то тоже отмечаем её проходимой
                                 //Сначала смотрим, является ли точка первой в линейке
                                 //Если нет
                                 if (prev_point > 0) {
                                     //Если перед ней нет точки
                                     if (dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point - 1)* POINTS_DIM + 0] == 0 &&
                                             dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point - 1)* POINTS_DIM + 1] == 0 &&
                                             dev_p[tid * LASER_NMB * POINTS_DIM + (prev_point - 1)* POINTS_DIM + 2] == 0)
                                     {
                                         //Отмечаем точку проходимой
                                         dev_pass[tid * LASER_NMB + prev_point] = true;
                                     }
                                 }
                                 //Если да
                                 else {
                                     //Отмечаем точку проходимой
                                     dev_pass[tid * LASER_NMB + prev_point] = true;

                                 }

                        }
                        //Иначе - точка непроходимая
                            else {
//                                PA->setPixelValueInMeterCoordinates(x + PA->getPixelHeight() / (2*PA->getMapPixelScale()),y+ PA->getPixelWidth() / (2*PA->getMapPixelScale()),0);
                            }

                       }
                      }
                    //Если отфильтрована, то считаем проходимой
                    else {
                        dev_pass[tid * LASER_NMB + j] = true;
                    }
                }
                //Инкрементируем нить
                tid += blockDim.x*gridDim.x;
    }

}

void processPassability_CUDA(bool *dev_passab,
                             int *host_p_size,
                             int *dev_p_size,
                             float *dev_p,
                             float angleTrsh) {

    //Вычисляем количество пакетов в облаке
    int packetsNmb = (*host_p_size)/LASER_NMB;

    //Вычисляем число блоков
    int blocksPerGrid = ((packetsNmb + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);

    //Выделяем память под количество пакетов
    int *dev_packetsNmb;
    cudaMalloc((void**)&dev_packetsNmb, sizeof(int));
    //Копируем значение на память устройства
    cudaMemcpy(dev_packetsNmb, &packetsNmb, sizeof(int), cudaMemcpyHostToDevice);

    //Выделяем память под порог по углу
    float *dev_angleTresh;
    cudaMalloc((void**)&dev_angleTresh, sizeof(float));
    //Копируем значение на память устройства
    cudaMemcpy(dev_angleTresh, &angleTrsh, sizeof(float), cudaMemcpyHostToDevice);

    //Передаём в функцию для параллельных вычислений
    computePassability<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_p, dev_p_size, dev_passab, dev_packetsNmb, dev_angleTresh);


}

