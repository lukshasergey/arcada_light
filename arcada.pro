TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CUDA_DIR      = /usr/local/cuda-6.5

INCLUDEPATH += /opt/ros/indigo/include
INCLUDEPATH += ../../devel/include
INCLUDEPATH += /usr/include/pcl-1.7
INCLUDEPATH += /usr/include
INCLUDEPATH  += $$CUDA_DIR/include
INCLUDEPATH += ~/catkin_ws/devel/include

LIBS +=  -lpcap \
     += L/usr/local/cuda-6.5/lib64 -lcudart -lcuda \

# новые package добавлять сюда

PACKAGES = arcada_velodyne_grabber \
        += arcada_occupancy_processing \
        += arcada_cloud_filter \
        += arcada_icp \


for(dir, PACKAGES) {
     exists($$dir) {
        SOURCES += $$dir/src/*.*
        SOURCES += $$dir/msg/*.msg
        SOURCES += $$dir/srv/*.srv
        SOURCES += $$dir/*.*
     }
}

HEADERS +=

SOURCES +=


