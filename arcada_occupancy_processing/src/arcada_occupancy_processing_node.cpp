#include <ros/ros.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include <fstream>
#include <sys/types.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <iomanip>

#include <cuda.h>
#include <cuda_runtime.h>

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include "sensor_msgs/ChannelFloat32.h"
#include "nav_msgs/OccupancyGrid.h"

#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3

sensor_msgs::PointCloud pointCloudMain;

using namespace std;

//--------------------УКАЗАТЕЛИ НА МАССИВЫ ДАННЫХ------------
static float *dev_pointCloud;
static bool *dev_passability;
static int *dev_size;
static float *host_pointCloud;
static bool *host_passability;
static int *host_size;
//----------------------------------------------------------

//------------------------ФЛАГИ---------------------------------------------------------
//Флаг на разрешение обработки данных
static bool isAllowToProcess;
static bool isProcessingPerforming;
static bool isReceivedCloud;
//--------------------------------------------------------------------------------------

//Объявление функции анализа проходимости на видеокарте
void processPassability_CUDA(bool *dev_passab,
                             int *host_p_size,
                             int *dev_p_size,
                             float *dev_p,
                             float angleTrsh);

//Функция принятия облака точек
void pointCloud_callback(const sensor_msgs::PointCloud &pointCloud) {

    if(isAllowToProcess) {

        isProcessingPerforming = true;
        ROS_INFO("Receive cloud");

        *host_size = pointCloud.points.size();
        //Переносим все параметры
        for(int i = 0; i < pointCloud.points.size(); i++) {
            host_pointCloud[POINT_DIM * i + 0] = pointCloud.points[i].x;
            host_pointCloud[POINT_DIM * i + 1] = pointCloud.points[i].y;
            host_pointCloud[POINT_DIM * i + 2] = pointCloud.points[i].z;
        }
        //Копируем на память устройства
        cudaMemcpy(dev_pointCloud, host_pointCloud, *host_size * POINTS_DIM * sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(dev_size, host_size, sizeof(int), cudaMemcpyHostToDevice);
        isReceivedCloud = true;
        isProcessingPerforming = false;
    }


}

//Функция окончания работы
void mySigintHandler(int sig) {

    //Ждём, пока не снимется флаг с обработки
    while(isProcessingPerforming) {
       usleep(1);
    }
    isAllowToProcess = false;

    //Освобождаем память
    cudaFree(dev_pointCloud);
    cudaFree(dev_passability);
    cudaFree(dev_size);

    free(host_passability);
    free(host_pointCloud);
    free(host_size);


    ROS_INFO("OCCUPANCY PROCESSING stopped");

  ros::shutdown();
}



int main(int argc, char **argv) {

    //Начальная инициализация узла
    ros::init(argc, argv, "arcada_occupancy_processing_node");
    ros::NodeHandle n;
    std::string frame_id;
    n.param<std::string> ("frame_id", frame_id, "/arcada_occupancy_processing");

    //-----------------------УКАЗАТЕЛИ НА ПЕРЕСЫЛАЕМЫЕ И ПРИНИМАЕМЫЕ ДАННЫЕ----------
    nav_msgs::OccupancyGrid OccupancyMap;
    //Подписчик на облако точек
    ros::Subscriber pointCloud_sub = n.subscribe("arcada/velodyne_pointCloud",32, pointCloud_callback);
    //Публикатор карты проходимости
    ros::Publisher occupancyPub = n.advertise<nav_msgs::OccupancyGrid>("arcada/occupancy_map",4);
    //-------------------------------------------------------------------------------

    //-----------------------ВЫДЕЛЕНИЕ ПАМЯТИ ПОД МАССИВЫ ДАННЫХ--------------------
    cudaMalloc((void**)&dev_pointCloud, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_passability, N_MAX*sizeof(bool));
    cudaMalloc((void**)&dev_size, sizeof(int));

    host_passability = (bool *) malloc(N_MAX * sizeof(bool));
    host_pointCloud = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_size = (int *) malloc(sizeof(int));
    //------------------------------------------------------------------------------

    //-----------------------УСТАНОВКА ПАРАМЕТРОВ КАРТЫ ПРОХОДИМОСТИ-----------------
    OccupancyMap.info.height = 500;
    OccupancyMap.info.width = 500;
    OccupancyMap.info.resolution = 0.04;
    OccupancyMap.data.resize(OccupancyMap.info.width * OccupancyMap.info.height);
    //-------------------------------------------------------------------------------


    //Сообщение о запуске
    ROS_INFO("OCCUPANCY PROCESSING started");

    //Функция при завершении работы
    signal(SIGINT, mySigintHandler);

    //Частота работы
    ros::Rate rate(10); //Hz

    //Разрешаем обработку
    isAllowToProcess = true;



    while(ros::ok()) {

        //Выполняем обработку
        if(isAllowToProcess && isReceivedCloud) {
            isProcessingPerforming = true;
            isAllowToProcess = false;

            //Обработка
            //Отправляем обработку на графический процессор
            processPassability_CUDA(dev_passability,
                                         host_size,
                                         dev_size,
                                         dev_pointCloud,
                                         0.6);

            //Копируем информацию о проходимости с памяти устройства
            cudaMemcpy(host_passability, dev_passability, *host_size * sizeof(bool), cudaMemcpyDeviceToHost);

            //Заполняем непосредственно карту проходимости
            //Сначала заполняем её серым цветом
            for(int x = 0; x < OccupancyMap.info.width; x++) {
                for(int y = 0; y < OccupancyMap.info.height; y++) {
                    OccupancyMap.data[y * OccupancyMap.info.width + x] = 50;
                }
            }
            for(int i = 0; i <*host_size; i++) {
                //Проверяем, является ли непроходимой точка
                if(host_passability[i] == false) {
                    //Тогда проецируем эту точку
                    //Вычисляем координаты
                    float x = host_pointCloud[i * POINTS_DIM + 0];
                    float y = host_pointCloud[i * POINTS_DIM + 1];
                    int x_occup = x/OccupancyMap.info.resolution + OccupancyMap.info.width/2;
                    int y_occup = y/OccupancyMap.info.resolution + OccupancyMap.info.height/2;
                    if(x_occup > 0 && x_occup < OccupancyMap.info.width && y_occup > 0 && y_occup < OccupancyMap.info.height) {
                        OccupancyMap.data[y_occup * OccupancyMap.info.width + x_occup] = 100;
                    }
                }
            }
            //В цикле перебираем все точки карты проходимости в поиске непроходимых точек
            int zeroPointPixelX = OccupancyMap.info.width/2;
            int zeroPointPixelY = OccupancyMap.info.height/2;
            cout<<"Zero point "<<zeroPointPixelX<<" "<<zeroPointPixelY<<endl;
            for (int x = 0; x < OccupancyMap.info.width; x++) {
                for (int y = 0; y < OccupancyMap.info.height; y++) {


                    //Проверяем, не является ли это точкой старта
                    if(x != zeroPointPixelX && y != zeroPointPixelY) {

                        //Определяем, непроходима ли точка
                        if(OccupancyMap.data[y * OccupancyMap.info.width + x] > 90 /*|| x == 0 || x == (pixelWidth-1) || y == 0 || y == (pixelHeight-1)*/) {

                            //Определяем уравнение прямой, проходящей через эту точку и точку начала сканирования
                            float k = ((float)y - (float)zeroPointPixelY)/((float)x - (float)zeroPointPixelX);
                            float b = (float)y - k * (float)x;

                            //Теперь, работая в декартовой системе координат с центром карты в качестве нуля, разбиваем координатную плоскость на две части
                            if(x >= zeroPointPixelX /*&& y >= zeroPointPixelY*/) {

                                //В этом случае используем цикл от нулевой точки до препятствия
                                //Задаём начальные условия
                                float currentPointX = zeroPointPixelX;
                                float currentPointY = zeroPointPixelY;
                                float stepX;
                                if(abs(k)>1)
                                    stepX=1/k;
                                else
                                    stepX = 1;
                                //Суть цикла в том, что мы идём по пути луча до тех пор, пока не встретим препятствие
                                while(OccupancyMap.data[(int)currentPointY * OccupancyMap.info.width + (int)currentPointX] < 90 &&
                                      currentPointX < OccupancyMap.info.width &&
                                      currentPointY < OccupancyMap.info.height &&
                                      currentPointX > 0 &&
                                      currentPointY > 0) {
                                    OccupancyMap.data[(int)currentPointY * OccupancyMap.info.width + (int)currentPointX] = 0;
                                    currentPointX = currentPointX + stepX;
                                    currentPointY = currentPointX * k + b;
                                }

                            }
                            if(x < zeroPointPixelX /*&& y >= zeroPointPixelY*/) {

                                //В этом случае используем цикл от нулевой точки до препятствия
                                //Задаём начальные условия
                                float currentPointX = zeroPointPixelX;
                                float currentPointY = zeroPointPixelY;
                                float stepX;
                                if(abs(k)>1)
                                    stepX=1/k;
                                else
                                    stepX = 1;
                                //Суть цикла в том, что мы идём по пути луча до тех пор, пока не встретим препятствие
                                while(OccupancyMap.data[(int)currentPointY * OccupancyMap.info.width + (int)currentPointX] < 90 &&
                                      currentPointX < OccupancyMap.info.width &&
                                      currentPointY < OccupancyMap.info.height &&
                                      currentPointX > 0 &&
                                      currentPointY > 0) {
                                    OccupancyMap.data[(int)currentPointY * OccupancyMap.info.width + (int)currentPointX] = 0;
                                    currentPointX = currentPointX - stepX;
                                    currentPointY = currentPointX * k + b;
                                }

                            }
                        }
                    }
                }
            }
            //Публикуем карту
            ROS_INFO("Publish map");
            OccupancyMap.header.frame_id = frame_id;
            OccupancyMap.header.stamp = ros::Time::now();
            OccupancyMap.info.origin.position.x = 0 - (OccupancyMap.info.width/2)*OccupancyMap.info.resolution;
            OccupancyMap.info.origin.position.y = 0 - (OccupancyMap.info.height/2)*OccupancyMap.info.resolution;
            occupancyPub.publish(OccupancyMap);
            
            //Устанавливаем флаги
            isAllowToProcess = true;
            isReceivedCloud = false;
            isProcessingPerforming = false;
        }

        ros::spinOnce();
        rate.sleep();
    }


    ROS_INFO("OCCUPANCY PROCESSING finished");
    return 0;

}
