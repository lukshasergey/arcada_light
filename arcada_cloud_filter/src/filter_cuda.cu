//#include <ros/ros.h>
//#include <unistd.h>

//#include <fstream>
//#include <sys/types.h>
//#include <stdlib.h>
//#include <sstream>
//#include <string>
//#include <iomanip>

//#include "sensor_msgs/Imu.h"
//#include "sensor_msgs/JointState.h"
//#include "sensor_msgs/LaserScan.h"
//#include "sensor_msgs/PointCloud.h"
//#include "std_msgs/Float32.h"
//#include "std_msgs/String.h"
//#include "sensor_msgs/ChannelFloat32.h"
//#include "nav_msgs/OccupancyGrid.h"

#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>

#define THREADS_PER_BLOCK 1024
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3
#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925

//Функция для оценки проходимости точки
__global__ void filterCloud(float *pointCloud, int *p_size, int *intensity, float *rtp_height, float *rtp_forward, float *rtp_backward, float *rtp_side, float *lowLewel, float *hightLevel, float *maxRange) {

    //Вычисляем номер нити
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    while(tid < *p_size) {

                //Корректируем высоту
                pointCloud[tid * POINTS_DIM + 2] = pointCloud[tid * POINTS_DIM + 2] + *rtp_height;

                //Удаляем точки РТП
                if (abs(pointCloud[tid*POINTS_DIM + 0]) < *rtp_side && (pointCloud[tid*POINTS_DIM + 1] < *rtp_forward && pointCloud[tid*POINTS_DIM + 1] > (-*rtp_backward))) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }

                //Удаление точек по верхнему или нижнему уровню
                if (pointCloud[tid*3 + 2] < *lowLewel || pointCloud[tid*3 + 2] > *hightLevel) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }

                //Фильтрация точек по дальности
                float distance = sqrt(pointCloud[tid*3 + 0]*pointCloud[tid*3 + 0] + pointCloud[tid*3 + 1]*pointCloud[tid*3 + 1] + pointCloud[tid*3 + 2]*pointCloud[tid*3 + 2]);
                if (distance > *maxRange) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }


                //Инкрементируем нить
                tid += blockDim.x*gridDim.x;
    }
    syncthreads();

}

void filterCloud_CUDA(float *dev_pointCloud,
                      int *host_p_size,
                      int *dev_p_size,
                      int *dev_intensity,
                      float rtp_height,
                      float rtp_forward,
                      float rtp_backward,
                      float rtp_side,
                      float lowLevel,
                      float hightLevel,
                      float maxRange) {

    //Вычисляем число блоков
    int blocksPerGrid = ((*host_p_size + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);

    //Указатели на параметры
    float *dev_rtp_height, *dev_rtp_forward, *dev_rtp_backward, *dev_rtp_side, *dev_lowLewel, *dev_hightLevel, *dev_maxRange;

    //Выделяем память под параметры фильтрации
    cudaMalloc((void**)&dev_rtp_height, sizeof(float));
    cudaMemcpy(dev_rtp_height, &rtp_height, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_forward, sizeof(float));
    cudaMemcpy(dev_rtp_forward, &rtp_forward, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_backward, sizeof(float));
    cudaMemcpy(dev_rtp_backward, &rtp_backward, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_side, sizeof(float));
    cudaMemcpy(dev_rtp_side, &rtp_side, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_lowLewel, sizeof(float));
    cudaMemcpy(dev_lowLewel, &lowLevel, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_hightLevel, sizeof(float));
    cudaMemcpy(dev_hightLevel, &hightLevel, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_maxRange, sizeof(float));
    cudaMemcpy(dev_maxRange, &maxRange, sizeof(float),cudaMemcpyHostToDevice);

    filterCloud<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_pointCloud, dev_p_size, dev_intensity, dev_rtp_height, dev_rtp_forward, dev_rtp_backward, dev_rtp_side, dev_lowLewel, dev_hightLevel, dev_maxRange);

}

