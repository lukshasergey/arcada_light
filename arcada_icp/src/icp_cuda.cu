//#include <ros/ros.h>
//#include <unistd.h>

//#include <fstream>
//#include <sys/types.h>
//#include <stdlib.h>
//#include <sstream>
//#include <string>
//#include <iomanip>

//#include "sensor_msgs/Imu.h"
//#include "sensor_msgs/JointState.h"
//#include "sensor_msgs/LaserScan.h"
//#include "sensor_msgs/PointCloud.h"
//#include "std_msgs/Float32.h"
//#include "std_msgs/String.h"
//#include "sensor_msgs/ChannelFloat32.h"
//#include "nav_msgs/OccupancyGrid.h"

#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#define THREADS_PER_BLOCK 1024
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3
#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925

//Функция для оценки проходимости точки
__global__ void filterCloud(float *pointCloud, int *p_size, int *intensity, float *rtp_height, float *rtp_forward, float *rtp_backward, float *rtp_side, float *lowLewel, float *hightLevel, float *maxRange, float *initX, float *initY) {

    //Вычисляем номер нити
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    while(tid < *p_size) {

                //Корректируем высоту
                pointCloud[tid * POINTS_DIM + 2] = pointCloud[tid * POINTS_DIM + 2] + *rtp_height;

                //Корректируем положение
                pointCloud[tid * POINTS_DIM + 0] = pointCloud[tid * POINTS_DIM + 0] + *initX;
                pointCloud[tid * POINTS_DIM + 1] = pointCloud[tid * POINTS_DIM + 1] + *initY;

                //Удаляем точки РТП
                if (abs(pointCloud[tid*POINTS_DIM + 0]) < *rtp_side && (pointCloud[tid*POINTS_DIM + 1] < *rtp_forward && pointCloud[tid*POINTS_DIM + 1] > (-*rtp_backward))) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }

                //Удаление точек по верхнему или нижнему уровню
                if (pointCloud[tid*3 + 2] < *lowLewel || pointCloud[tid*3 + 2] > *hightLevel) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }

                //Фильтрация точек по дальности
                float distance = sqrt(pointCloud[tid*3 + 0]*pointCloud[tid*3 + 0] + pointCloud[tid*3 + 1]*pointCloud[tid*3 + 1] + pointCloud[tid*3 + 2]*pointCloud[tid*3 + 2]);
                if (distance > *maxRange) {
                    //Приводим точки в ноль
                    pointCloud[tid*3 + 0] = 0;
                    pointCloud[tid*3 + 1] = 0;
                    pointCloud[tid*3 + 2] = 0;

                    intensity[tid] = 0;
                    intensity[tid] = 0;
                    intensity[tid] = 0;
                }


                //Инкрементируем нить
                tid += blockDim.x*gridDim.x;
    }
    syncthreads();

}

///-------------------------------------------------------Функции, выполняемые на видеокарте------------------------------------------------


//Метод грубой силы
__global__ void NNSearchBF(float *p1, float *p2, int *npIndex, int *p1_size, int *p2_size, float maxNNdist, float *minDist, int *intensity2, int *intensity1) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int nI;
    float p2_1, p2_2, p2_3;
    float distance;
    while (tid < *p2_size) {
    //-------Тупой метод нахождения расстояния
        //--------Описание метода------------
        //tid - номер строки матрицы, m - номер столбца
        //Тогда, в каждой tid-й строке будут все расстояния от tid-й точки во втором облаке до каждой точки в первом облаке
        //Находя минимальное значение в каждой строке, мы определяем, какая из точек первого облака наиболее близка к tid-й точки второго облака, при этом запоминаем индекс этой точки
        minDist[tid] = 99999;
        npIndex[tid] = 101010;

        p2_1 = p2[tid*3 + 0];
        p2_2 = p2[tid*3 + 1];
        p2_3 = p2[tid*3 + 2];
        //Интенсивность
        int intens2 = intensity2[tid];
        //Првоеряем, не отфильтрована ли точка
        if( p2_1 != 0 || p2_2 != 0 || p2_3 != 0) {
            //Вычисляем диапазон поиска ближайшей точки в соответствии с вращением Velodyne
            int low_nmb, high_nmb;
            int coeff = 75;
            //Проверяем, не выходит ли за диапазон
            if (tid < LASER_NMB*coeff)
                low_nmb = tid + (*p1_size) - LASER_NMB*coeff;
            else
                low_nmb = tid - LASER_NMB*coeff;
            if((tid + LASER_NMB * coeff) > (*p1_size))
                high_nmb = tid - (*p1_size) + LASER_NMB*coeff;
            else
                high_nmb = tid + LASER_NMB*coeff;

            //Если не отфильтрофана, то для каждой точки вычисляем
            for (int m = low_nmb; m < high_nmb; m++) {
                //Проверяем, не отфильтрована ли следующая точка
                if(p1[m*3 + 0] != 0 || p1[m*3 + 1] != 0 || p1[m*3 + 2] != 0) {
                    distance = sqrt((p2_1 - p1[m*3 + 0])*(p2_1 - p1[m*3 + 0]) + (p2_2 - p1[m*3 + 1])*(p2_2 - p1[m*3 + 1]) + (p2_3 - p1[m*3 + 2])*(p2_3 - p1[m*3 + 2]));
                    if(distance < minDist[tid] && distance < maxNNdist && abs(intensity1[m] - intens2) < 7) {
                        minDist[tid] = distance;
                        npIndex[tid] = m+1;
                    }
                }
            }
        }
        tid += blockDim.x*gridDim.x;

    }
}


//Функция вычисления среднего значения среди найденных ближайших точек
__global__ void NNdistanceAverageCalc(float *minDist, float *mean_dist, int *NPnumber, int *p2_size) {

    //Расшаренные переменные
    __shared__ float sum[THREADS_PER_BLOCK];
    __shared__ int num[THREADS_PER_BLOCK];
    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Вычисление индекса расшаренной переменной блока
    int cacheIndex = threadIdx.x;

    //Суммирование в цикле
    while(tid < *p2_size) {
        //Проверяем, найденна ли ближайшая точка
        //Кодовое значение 99999
        if (minDist[tid] != 99999) {
            //В таком случае суммируем
            sum[cacheIndex] += minDist[tid];
            num[cacheIndex]++;
        }
        //Инкрементируем индекс
        tid += blockDim.x*gridDim.x;
    }

    //Синхронизируем нити в блоке
    __syncthreads();

    //Выполняем редукцию в блоке
    //Условие выполнения - TREADS PER BLOCK - степень двойки
    int i = blockDim.x/2;
    while (i !=0 ) {
        if (cacheIndex < i) {
            sum[cacheIndex] += sum[cacheIndex + i];
            num[cacheIndex] += num[cacheIndex + i];
        }
        __syncthreads();
        i /= 2;
    }

    //Записываем полученное значение
    if (cacheIndex == 0) {
        mean_dist[blockIdx.x] = sum[0];
        NPnumber[blockIdx.x] = num[0];
    }

}

//Функция для окончательного суммирования и вычисления среднего значения расстояния между ближайшими точками
__global__ void NNdistAverageFinalCalc(float *mean_dist, int *NPnumber, int *p2_size) {

    //Вычисляем количество использованных блоков
    int blocks = (((*p2_size) + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);

    float sum = 0;
    int num = 0;
    for (int i = 0; i < blocks; i++) {
        sum = sum + mean_dist[i];
        num = num + NPnumber[i];
    }

    //Найденное значение записываем в первые ячейки массивов
    NPnumber[0] = num;
    mean_dist[0] = sum/NPnumber[0];
}


//Функция для вычисления и применения перемещений
__global__ void applyTranslation(float *p, float *mean1, float *mean2, int *p_size, float *transformation) {
    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Первоначально вычисляем перемещение как разность между ссответсвующими элементами векторов
    float transl[3];
    transl[0] = mean2[0] - mean1[0];
    transl[1] = mean2[1] - mean1[1];
    transl[2] = mean2[2] - mean1[2];
    //Далее, осуществляем применение перемещения
    //Суммирование в цикле
    while (tid < *p_size) {
        if (p[tid*3 + 0] != 0 || p[tid*3 + 1] != 0 || p[tid*3 + 2] != 0) {
            p[tid*3 + 0] = p[tid*3 + 0] + transl[0];
            p[tid*3 + 1] = p[tid*3 + 1] + transl[1];
            p[tid*3 + 2] = p[tid*3 + 2] + transl[2];
        }

        tid += blockDim.x*gridDim.x;
    }
    //Возвращаем занчения трансформации в первых трёх элементах вектора
    transformation[0] = transl[0];
    transformation[1] = transl[1];
    transformation[2] = transl[2];

}

//Функция для вычисления центроидов сразу двух облаков
__global__ void centroidsDualCalc(int *p1_size, int *p2_size, float *p1, float *p2, int *npIndex, int *NPnumber, float *mean1, float *mean2) {

    //Расшаренные переменные временных значений
    __shared__ float cache1[THREADS_PER_BLOCK*3];
    __shared__ float cache2[THREADS_PER_BLOCK*3];
    //Переменная для подсчёта количества соответсвующих точек
    __shared__ int NPNcache[THREADS_PER_BLOCK];
    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Номер точки
    int pointNmb;
    //Вычисление индекса расшаренной переменной блока
    int cacheIndex = threadIdx.x;
    //Временный вектор средних значений
    float temp1[3] = {0, 0, 0};
    float temp2[3] = {0, 0, 0};
    //Временное сумарное количество соответствующих точек
    int tmpNPN = 0;
    //Суммирование в цикле
    //Проходим по всем точкам второго облака
    while (tid < *p2_size) {
        //Проверяем, не отфильтрована ли точка второго облака
        if (p2[tid*3 + 0] != 0 || p2[tid*3 + 1] != 0 || p2[tid*3 + 2] != 0) {

            //Проверяем на наличие ближайшей точки
            //Если для данной точки не существует или не найдена ближайшая, о присваиваем условное значение 101010
            if (npIndex[tid] != npIndex[tid] || npIndex[tid] == 101010 || npIndex[tid] > *p1_size)
                pointNmb = 101010;
            //В противном случае присваиваем индекс - 1
            else
                pointNmb = npIndex[tid]-1;

            //Проверяем условие, есть ли ближайшая точка, а также, не отфильтрована ли точка из первого облака
            if (pointNmb != 101010) {
                if (p1[pointNmb*3 + 0] != 0 || p1[pointNmb*3 + 1] != 0 || p1[pointNmb*3 + 2] != 0) {

                    //Если всё нормально, то суммируем для первого облака
                    temp1[0] += p1[pointNmb*3 + 0];
                    temp1[1] += p1[pointNmb*3 + 1];
                    temp1[2] += p1[pointNmb*3 + 2];

                    //А затем и для второго
                    temp2[0] += p2[tid*3 + 0];
                    temp2[1] += p2[tid*3 + 1];
                    temp2[2] += p2[tid*3 + 2];
                    //Инкрементируем количество просуммированных точек
                    tmpNPN++;
                }
            }

        }
        //Переходим к следующей нити
        tid += blockDim.x*gridDim.x;
    }

    //После окончания цикла переводим временных значения в кэш
    //Для первого облака
    cache1[cacheIndex*3 + 0] = temp1[0];
    cache1[cacheIndex*3 + 1] = temp1[1];
    cache1[cacheIndex*3 + 2] = temp1[2];
    //Для второго облака
    cache2[cacheIndex*3 + 0] = temp2[0];
    cache2[cacheIndex*3 + 1] = temp2[1];
    cache2[cacheIndex*3 + 2] = temp2[2];
    //Общее количество точек
    NPNcache[cacheIndex] = tmpNPN;

    //Синхронизируем нити в блоке
    __syncthreads();

    //Выполняем редукцию в блоке
    //Условие выполнения - TREADS PER BLOCK - степень двойки
    int i = blockDim.x/2;
    while (i !=0 ) {
        if (cacheIndex < i) {
            //Первое облако
            cache1[cacheIndex*3 + 0] += cache1[cacheIndex*3 + 0 + i*3];
            cache1[cacheIndex*3 + 1] += cache1[cacheIndex*3 + 1 + i*3];
            cache1[cacheIndex*3 + 2] += cache1[cacheIndex*3 + 2 + i*3];
            //Второе облако
            cache2[cacheIndex*3 + 0] += cache2[cacheIndex*3 + 0 + i*3];
            cache2[cacheIndex*3 + 1] += cache2[cacheIndex*3 + 1 + i*3];
            cache2[cacheIndex*3 + 2] += cache2[cacheIndex*3 + 2 + i*3];
            //Количество просуммированных точек
            NPNcache[cacheIndex] += NPNcache[cacheIndex + i];
        }
        //Синхронизируем нити
        __syncthreads();
        i /= 2;
    }

    //Записываем полученное значение в массив
    if (cacheIndex == 0) {
        //Для первого облака
        mean1[blockIdx.x*3 + 0] = cache1[0];
        mean1[blockIdx.x*3 + 1] = cache1[1];
        mean1[blockIdx.x*3 + 2] = cache1[2];
        //Для второго облака
        mean2[blockIdx.x*3 + 0] = cache2[0];
        mean2[blockIdx.x*3 + 1] = cache2[1];
        mean2[blockIdx.x*3 + 2] = cache2[2];
        //Общее количество
        NPnumber[blockIdx.x] = NPNcache[0];
    }

}

//Функция для окончательного сложения при вычислении центроида двух облаков

__global__ void centrDualFinalCalc(float *mean1, float *mean2, int *NPnumber, int *p2_size) {

    //Вычисляем количество использованных блоков
    int blocks = (((*p2_size) + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);
    //Временная переменная для суммирования
    float temp1[3] = {0, 0, 0};
    float temp2[3] = {0, 0, 0};
    //Количество просуммированных точек
    int tmpNPN = 0;
    //В цикле выполняем окончательное сложение
    for (int i = 0; i < blocks; i++) {
        //Для первого облака
        temp1[0] = temp1[0] + mean1[i*3 + 0];
        temp1[1] = temp1[1] + mean1[i*3 + 1];
        temp1[2] = temp1[2] + mean1[i*3 + 2];
        //Для второго облака
        temp2[0] = temp2[0] + mean2[i*3 + 0];
        temp2[1] = temp2[1] + mean2[i*3 + 1];
        temp2[2] = temp2[2] + mean2[i*3 + 2];
        //Общее количество точек
        tmpNPN = tmpNPN + NPnumber[i];
    }

    //Найденный центроид записываем в первые 3 ячейки вектора центроидов
    NPnumber[0] = tmpNPN;
    //Для первого центроида
    mean1[0] = temp1[0]/NPnumber[0];
    mean1[1] = temp1[1]/NPnumber[0];
    mean1[2] = temp1[2]/NPnumber[0];
    //Для второго центроида
    mean2[0] = temp2[0]/NPnumber[0];
    mean2[1] = temp2[1]/NPnumber[0];
    mean2[2] = temp2[2]/NPnumber[0];
}



//Функция для вычисления матрицы для сингулярного разложения
__global__ void matrixSVDcalc(float *M2, float *p1, float *p2, float *mean1, float *mean2, int *npIndex, int *p1_size, int *p2_size) {

    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Расшаренная переменная
    __shared__ float cacheM2[THREADS_PER_BLOCK*9];
    //Индекс первого облака
    //Вводим условие для отсечения лишних точек
    int ind1;
    //Вычисление итерации сложения
    int N = 0;
    if (*p1_size < *p2_size)
        N = *p1_size;
    else
        N = *p2_size;

    //Файл для временного хранения матрицы
    float tmpM2[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    //Вычисление индекса расшаренной переменной блока
    int cacheIndex = threadIdx.x;
    while (tid < *p2_size) {
        //Проверка, найдена ли ближайшая точка
        if(npIndex[tid] != 101010)
            ind1 = npIndex[tid] - 1;
        else
            ind1 = npIndex[tid];
        if (ind1 != 101010) {
            tmpM2[0] = tmpM2[0] + (p1[ind1*3 + 0] - mean1[0]) * (p2[tid*3 + 0] - mean2[0]);
            tmpM2[1] = tmpM2[1] + (p1[ind1*3 + 0] - mean1[0]) * (p2[tid*3 + 1] - mean2[1]);
            tmpM2[2] = tmpM2[2] + (p1[ind1*3 + 0] - mean1[0]) * (p2[tid*3 + 2] - mean2[2]);
            tmpM2[3] = tmpM2[3] + (p1[ind1*3 + 1] - mean1[1]) * (p2[tid*3 + 0] - mean2[0]);
            tmpM2[4] = tmpM2[4] + (p1[ind1*3 + 1] - mean1[1]) * (p2[tid*3 + 1] - mean2[1]);
            tmpM2[5] = tmpM2[5] + (p1[ind1*3 + 1] - mean1[1]) * (p2[tid*3 + 2] - mean2[2]);
            tmpM2[6] = tmpM2[6] + (p1[ind1*3 + 2] - mean1[2]) * (p2[tid*3 + 0] - mean2[0]);
            tmpM2[7] = tmpM2[7] + (p1[ind1*3 + 2] - mean1[2]) * (p2[tid*3 + 1] - mean2[1]);
            tmpM2[8] = tmpM2[8] + (p1[ind1*3 + 2] - mean1[2]) * (p2[tid*3 + 2] - mean2[2]);
        }

        tid += blockDim.x*gridDim.x;
    }

    //Перенос в кэш

    cacheM2[cacheIndex*9 + 0] = tmpM2[0];
    cacheM2[cacheIndex*9 + 1] = tmpM2[1];
    cacheM2[cacheIndex*9 + 2] = tmpM2[2];
    cacheM2[cacheIndex*9 + 3] = tmpM2[3];
    cacheM2[cacheIndex*9 + 4] = tmpM2[4];
    cacheM2[cacheIndex*9 + 5] = tmpM2[5];
    cacheM2[cacheIndex*9 + 6] = tmpM2[6];
    cacheM2[cacheIndex*9 + 7] = tmpM2[7];
    cacheM2[cacheIndex*9 + 8] = tmpM2[8];

    //Синхронизация потоков
    __syncthreads();

    //Выполняем редукцию в блоке
    //Условие выполнения - TREADS PER BLOCK - степень двойки
    int i = blockDim.x/2;
    while (i !=0 ) {
        if (cacheIndex < i) {
            cacheM2[cacheIndex*9 + 0] += cacheM2[cacheIndex*9 + 0 + i*9];
            cacheM2[cacheIndex*9 + 1] += cacheM2[cacheIndex*9 + 1 + i*9];
            cacheM2[cacheIndex*9 + 2] += cacheM2[cacheIndex*9 + 2 + i*9];
            cacheM2[cacheIndex*9 + 3] += cacheM2[cacheIndex*9 + 3 + i*9];
            cacheM2[cacheIndex*9 + 4] += cacheM2[cacheIndex*9 + 4 + i*9];
            cacheM2[cacheIndex*9 + 5] += cacheM2[cacheIndex*9 + 5 + i*9];
            cacheM2[cacheIndex*9 + 6] += cacheM2[cacheIndex*9 + 6 + i*9];
            cacheM2[cacheIndex*9 + 7] += cacheM2[cacheIndex*9 + 7 + i*9];
            cacheM2[cacheIndex*9 + 8] += cacheM2[cacheIndex*9 + 8 + i*9];
        }
        __syncthreads();
        i /= 2;
    }

    //Записываем полученное значение
    if (cacheIndex == 0) {
        M2[blockIdx.x*9 + 0] = cacheM2[0];
        M2[blockIdx.x*9 + 1] = cacheM2[1];
        M2[blockIdx.x*9 + 2] = cacheM2[2];
        M2[blockIdx.x*9 + 3] = cacheM2[3];
        M2[blockIdx.x*9 + 4] = cacheM2[4];
        M2[blockIdx.x*9 + 5] = cacheM2[5];
        M2[blockIdx.x*9 + 6] = cacheM2[6];
        M2[blockIdx.x*9 + 7] = cacheM2[7];
        M2[blockIdx.x*9 + 8] = cacheM2[8];
    }


}

//Функция для окончательного вычисления матрицы

__global__ void matrixSVDcalcFinal(float *M2, int blocks) {
    float temp[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (int i = 0; i < blocks; i++) {
        temp[0] = temp[0] + M2[i*9 + 0];
        temp[1] = temp[1] + M2[i*9 + 1];
        temp[2] = temp[2] + M2[i*9 + 2];
        temp[3] = temp[3] + M2[i*9 + 3];
        temp[4] = temp[4] + M2[i*9 + 4];
        temp[5] = temp[5] + M2[i*9 + 5];
        temp[6] = temp[6] + M2[i*9 + 6];
        temp[7] = temp[7] + M2[i*9 + 7];
        temp[8] = temp[8] + M2[i*9 + 8];
    }

    //Найденную матрицу записываем в первые 9 ячейки вектора М2
    M2[0] = temp[0];
    M2[1] = temp[1];
    M2[2] = temp[2];
    M2[3] = temp[3];
    M2[4] = temp[4];
    M2[5] = temp[5];
    M2[6] = temp[6];
    M2[7] = temp[7];
    M2[8] = temp[8];
};

//Функция применения вращения

__global__ void rotateCloud(float *p, float *R, int *p_size) {
    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Вводим временные переменные
    float point_x, point_y, point_z;
    //Далее, осуществляем применение вращения
    //Суммирование в цикле
    while (tid < *p_size) {
        //Преобразование и запись во временные переменные
        //Если точка не отфильтрована
        if (p[tid*3 + 0] != 0 || p[tid*3 + 1] != 0 || p[tid*3 + 2] != 0) {
            point_x = p[tid*3 + 0] * R[0] + p[tid*3 + 1] * R[3] + p[tid*3 + 2] * R[6];
            point_y = p[tid*3 + 0] * R[1] + p[tid*3 + 1] * R[4] + p[tid*3 + 2] * R[7];
            point_z = p[tid*3 + 0] * R[2] + p[tid*3 + 1] * R[5] + p[tid*3 + 2] * R[8];
            //Возврат обратно
            p[tid*3 + 0] = point_x;
            p[tid*3 + 1] = point_y;
            p[tid*3 + 2] = point_z;
        }
        tid += blockDim.x*gridDim.x;
    }
}


//Оценочная функция, вычисляющая среднее расстояние между соответствующими точками

__global__ void metricsDist(float *p1, float *p2, float *averDist, int *p1_size, int *p2_size) {

    __shared__ float cache[THREADS_PER_BLOCK];
    //Линейное вычисление индекса
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    //Вычисление индекса расшаренной переменной блока
    int cacheIndex = threadIdx.x;
    //Временный вектор средних значений
    float temp = 0;
    //Вычисление итерации сложения
    int N = 0;
    if (*p1_size < *p2_size)
        N = *p1_size;
    else
        N = *p2_size;
    //Суммирование в цикле
    while (tid < N) {
            temp = temp + sqrt((p2[tid*3 + 0] - p1[tid*3 + 0])*(p2[tid*3 + 0] - p1[tid*3 + 0]) + (p2[tid*3 + 1] - p1[tid*3 + 1])*(p2[tid*3 + 1] - p1[tid*3 + 1]) + (p2[tid*3 + 2] - p1[tid*3 + 2])*(p2[tid*3 + 2] - p1[tid*3 + 2]));
        tid += blockDim.x*gridDim.x;
    }

    cache[cacheIndex] = temp;

    //Синхронизируем нити в блоке
    __syncthreads();

    //Выполняем редукцию в блоке
    //Условие выполнения - TREADS PER BLOCK - степень двойки
    int i = blockDim.x/2;
    while (i !=0 ) {
        if (cacheIndex < i) {
            cache[cacheIndex] += cache[cacheIndex + i];
        }
        __syncthreads();
        i /= 2;
    }

    //Записываем полученное значение
    if (cacheIndex == 0) {
        averDist[blockIdx.x] = cache[0];
    }
}

//Функция окончательного вычисления критерия оценки
//Аналогично, цикл
__global__ void metricsDistFinal(float *averDist, int blocks, int *p1_size, int *p2_size) {
    //Вычисление итерации сложения
    int N = 0;
    if (*p1_size < *p2_size)
        N = *p1_size;
    else
        N = *p2_size;
    //Временное среднее значение
    float temp = 0;
    for (int i = 0; i < blocks; i++) {
        temp = temp + averDist[i];
    }

    //Найденное значение
    averDist[0] = temp/N;

}



void filterCloud_CUDA(float *dev_pointCloud,
                      int *host_p_size,
                      int *dev_p_size,
                      int *dev_intensity,
                      float rtp_height,
                      float rtp_forward,
                      float rtp_backward,
                      float rtp_side,
                      float lowLevel,
                      float hightLevel,
                      float maxRange,
                      float initX,
                      float initY) {

    //Вычисляем число блоков
    int blocksPerGrid = ((*host_p_size + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);

    //Указатели на параметры
    float *dev_rtp_height, *dev_rtp_forward, *dev_rtp_backward, *dev_rtp_side, *dev_lowLewel, *dev_hightLevel, *dev_maxRange, *dev_initX, *dev_initY;

    //Выделяем память под параметры фильтрации
    cudaMalloc((void**)&dev_rtp_height, sizeof(float));
    cudaMemcpy(dev_rtp_height, &rtp_height, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_forward, sizeof(float));
    cudaMemcpy(dev_rtp_forward, &rtp_forward, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_backward, sizeof(float));
    cudaMemcpy(dev_rtp_backward, &rtp_backward, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_rtp_side, sizeof(float));
    cudaMemcpy(dev_rtp_side, &rtp_side, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_lowLewel, sizeof(float));
    cudaMemcpy(dev_lowLewel, &lowLevel, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_hightLevel, sizeof(float));
    cudaMemcpy(dev_hightLevel, &hightLevel, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_maxRange, sizeof(float));
    cudaMemcpy(dev_maxRange, &maxRange, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_initX, sizeof(float));
    cudaMemcpy(dev_initX, &initX, sizeof(float),cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_initY, sizeof(float));
    cudaMemcpy(dev_initY, &initY, sizeof(float),cudaMemcpyHostToDevice);

    filterCloud<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_pointCloud, dev_p_size, dev_intensity, dev_rtp_height, dev_rtp_forward, dev_rtp_backward, dev_rtp_side, dev_lowLewel, dev_hightLevel, dev_maxRange, dev_initX, dev_initY);

}

void kernelICP(float *dev_p1,
               float *dev_p2,
               int *dev_p1_size,
               int *dev_p2_size,
               int *host_p1_size,
               int *host_p2_size,
               int *dev_npIndex,
               float *dev_mean_1,
               float *dev_mean_2,
               float *dev_M2,
               float *dev_R,
               int iteration_ICP,
               int *p1_seg,
               int dim_x,
               int dim_y,
               int dim_z,
               int segmentSize,
               int blocksPerGrid,
               int *dev_NPnumber,
               float *finalTransformation,
               float *dev_finalTransformation,
               Eigen::Quaternionf *QfinalTransf,
               float *dev_mindist,
               int *dev_intensity,
               int *dev_intensityPrev)
{
    std::cout<<"ICP kernel starts "<<"\n";

    bool isShowDebugData = false;


    //Реализация алгоритма ICP
    //Переменные времени этапов вычисления
    cudaEvent_t startMemAlloc, stopMemAlloc,  startInitCentrCalc, stopInitCentrCalc,  startApplyTranslation, stopApplyTranslation,  startNNSearch, stopNNSearch,  startCentroidCalc, stopCentroidCalc;
    cudaEvent_t startSVDcalc, stopSVDcalc,  startApplyingRotation, stopApplyingRotation, startCloudSegmantation, stopCloudSegmentation, startNormalsCalculation, stopNormalsCalculation;
    float elapsedTimeMemAlloc, elapsedTimeInitCentrCalc, elapsedTimeApplyTranslation, elapsedTimeNNSearch, elapsedTimeCentroidCalc, elapsedTimeSVDcalc, elapsedTimeApplyingRotation, elapsedTimeCloudSegmantation, elapsedTimeNormalsCalculation;

    //ПеременнаЯ количества точек первого и второго облака
    int p1_size = *host_p1_size;
    int p2_size = *host_p2_size;

    //Временный параметр для размещения матрицы поворота
    float *tmp_rotMatrix;
    tmp_rotMatrix = (float *) malloc(9 *sizeof(float));
    //Инициализация нулями параметров окончательного преобразования
    memset(finalTransformation, 0, 6*sizeof(float));

    //Установка в нули вектор преобразования
    cudaMemset(dev_finalTransformation, 0, 6*sizeof(float));



    //-------------------------------Вычисление и применение перемещения



    //Временные переменные для размещения преобразования
    float *tmp_transformation;
    tmp_transformation = (float *) malloc(6 *sizeof(float));
    //Временные переменные для размещения преобразования
    float *tmp_transformation2;
    tmp_transformation2 = (float *) malloc(6 *sizeof(float));
    //После применения перемещений отправляем полученные значения в вектор трансформации
    cudaMemcpy(tmp_transformation, dev_finalTransformation, 6*sizeof(float), cudaMemcpyDeviceToHost);


//    Инициализция нулями элементов массива сегментации первого облака
//    Инициализация нулями первых элементов подмассива
    for(int k = 0; k < dim_x*dim_y*dim_z*segmentSize; k++) {
            p1_seg[k] = 0;
    }


//    -------------------------------Основной цикл алгоритма



    for (int i = 0; i < iteration_ICP; i++) {


        //------------------------------Осуществляем поиск ближайших точек


        //Отметка времени
        cudaEventCreate(&startNNSearch);
        cudaEventCreate(&stopNNSearch);
        cudaEventRecord(startNNSearch, 0);

        //----------Сегментация облака
        //Осуществление разбиение облака точек на объёмы
        //Отметка времени



        //Альтернативный метод поиска грубой силой
        NNSearchBF<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_p1, dev_p2, dev_npIndex, dev_p1_size, dev_p2_size, 0.3, dev_mindist, dev_intensity, dev_intensityPrev);

        cudaEventRecord(stopNNSearch, 0);
        cudaEventSynchronize(stopNNSearch);
        cudaEventElapsedTime(&elapsedTimeNNSearch, startNNSearch, stopNNSearch);
        cudaEventDestroy(startNNSearch);
        cudaEventDestroy(stopNNSearch);

        if(isShowDebugData) {
            std::cout<<"NN search time "<<elapsedTimeNNSearch<<"\n";
        }


        //------------------------------Находим центроид
        //Отметка времени
        cudaEventCreate(&startCentroidCalc);
        cudaEventCreate(&stopCentroidCalc);
        cudaEventRecord(startCentroidCalc, 0);
        //Вычисление первого центроида
        cudaMemset(dev_NPnumber, 0, blocksPerGrid*sizeof(int));
        //Первый метод
        //centroidCalc<<<blocksPerGrid,THREADS_PER_BLOCK>>>(dev_p1, dev_mean_1, 1, dev_npIndex, dev_NPnumber, dev_p2_size);
        //Второй метод
        centroidsDualCalc<<<blocksPerGrid,THREADS_PER_BLOCK>>>(dev_p1_size, dev_p2_size, dev_p1, dev_p2, dev_npIndex, dev_NPnumber, dev_mean_1, dev_mean_2);


////        //Тестовый вывод частных центроидов
////        cudaMemcpy(host_mean_p1, dev_mean_2, 30000 * sizeof(float), cudaMemcpyDeviceToHost);

////        for(int j=1; j < 30000; j++) {
////            std::cout<<"Mean p1 "<<host_mean_p1[j]<<std::endl;
////        }

        //В результате вычисление мы имеем вектор, который нужно окончательно просуммировать
        //Чтобы не копировать данные в память CPU, сделаем это в одном потоке на GPU
        //Первый метод
        //centrFinalCalc<<<1,1>>>(dev_mean_1, blocksPerGrid, dev_NPnumber);
        //Второй метод
        centrDualFinalCalc<<<1,1>>>(dev_mean_1, dev_mean_2, dev_NPnumber, dev_p2_size);

        cudaEventRecord(stopCentroidCalc, 0);
        cudaEventSynchronize(stopCentroidCalc);
        cudaEventElapsedTime(&elapsedTimeCentroidCalc, startCentroidCalc, stopCentroidCalc);
        cudaEventDestroy(startCentroidCalc);
        cudaEventDestroy(stopCentroidCalc);

        if(isShowDebugData) {
            //Вывод значения центроида
            float *tmp;
            tmp = (float *) malloc(3*sizeof(float));
            cudaMemcpy(tmp, dev_mean_1, 3*sizeof(float), cudaMemcpyDeviceToHost);
            float *tmp2;
            tmp2 = (float *) malloc(3*sizeof(float));
            cudaMemcpy(tmp2, dev_mean_2, 3*sizeof(float), cudaMemcpyDeviceToHost);
            std::cout<<"Centroid 1 "<<tmp[0]<<" "<<tmp[1]<<" "<<tmp[2]<<"\n";
            std::cout<<"Centroid 2 "<<tmp2[0]<<" "<<tmp2[1]<<" "<<tmp2[2]<<"\n";

            std::cout<<"Centroid calculation time "<<elapsedTimeCentroidCalc<<"\n";

            //Очищение памяти
            free(tmp);
            free(tmp2);

        }

        //-----------------------------Вычисление матрицы для сингулярного разложение
        //Отметка времени
        cudaEventCreate(&startSVDcalc);
        cudaEventCreate(&stopSVDcalc);
        cudaEventRecord(startSVDcalc, 0);
        //Первоначальное
        matrixSVDcalc<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_M2, dev_p1, dev_p2, dev_mean_1, dev_mean_2, dev_npIndex, dev_p1_size, dev_p2_size);
        float M2[9], R[9];
        //Финальное
        matrixSVDcalcFinal<<<1,1>>>(dev_M2, blocksPerGrid);

        //------------------------------Выполнение сингулярного разложения
        //Оно будет выполняться на CPU
        //Копируем полученную матрицу M2
        cudaMemcpy(M2, dev_M2, 9*sizeof(float), cudaMemcpyDeviceToHost);
        Eigen::Matrix3f m;
        m(0,0) = M2[0];
        m(0,1) = M2[1];
        m(0,2) = M2[2];
        m(1,0) = M2[3];
        m(1,1) = M2[4];
        m(1,2) = M2[5];
        m(2,0) = M2[6];
        m(2,1) = M2[7];
        m(2,2) = M2[8];

        if(isShowDebugData) {
            //Выводим полученную матрицу
            std::cout<<"\nCovariation matrix \n";
            std::cout<<M2[0]<<"   "<<M2[1]<<"   "<<M2[2]<<"\n";
            std::cout<<M2[3]<<"   "<<M2[4]<<"   "<<M2[5]<<"\n";
            std::cout<<M2[6]<<"   "<<M2[7]<<"   "<<M2[8]<<"\n\n";
        }

        Eigen::JacobiSVD<Eigen::Matrix3f> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);
        const Eigen::Matrix3f U = svd.matrixU();
        const Eigen::Matrix3f V = svd.matrixV();
        //Нахождение преобразования
        Eigen::Matrix3f R_tmp = U * V.transpose();
        //Преобразовываем
        R[0] = R_tmp(0,0);
        R[1] = R_tmp(0,1);
        R[2] = R_tmp(0,2);
        R[3] = R_tmp(1,0);
        R[4] = R_tmp(1,1);
        R[5] = R_tmp(1,2);
        R[6] = R_tmp(2,0);
        R[7] = R_tmp(2,1);
        R[8] = R_tmp(2,2);
        //Копируем матрицу
        cudaMemcpy(dev_R, R, 9*sizeof(float), cudaMemcpyHostToDevice);
        //Квартенион текущего преобразования
        Eigen::Quaternionf QCurrent(R_tmp);

        //Суммирование углов поворота
        //Используется условие - для первой итерации - перенос текущего кватерниона
        if (i == 0) {

            *QfinalTransf = QCurrent;
        }
        //Для последующих - умножение текущего на предыдущии
        else {

            *QfinalTransf = *QfinalTransf * QCurrent;
        }



        cudaEventRecord(stopSVDcalc, 0);
        cudaEventSynchronize(stopSVDcalc);
        cudaEventElapsedTime(&elapsedTimeSVDcalc, startSVDcalc, stopSVDcalc);
        cudaEventDestroy(startSVDcalc);
        cudaEventDestroy(stopSVDcalc);

        if(isShowDebugData) {
            std::cout<<"SVD calculation time "<<elapsedTimeSVDcalc<<"\n";
        }

        //---------------------------------Применение вращения

        //Отметка времени
        cudaEventCreate(&startApplyingRotation);
        cudaEventCreate(&stopApplyingRotation);
        cudaEventRecord(startApplyingRotation, 0);

//        //Вывод значения облакак до вращения
//        cudaMemcpy(tmp, dev_p1, 3*sizeof(float), cudaMemcpyDeviceToHost);
//        std::cout<<"p1 before rotation "<<tmp[0]<<" "<<tmp[1]<<" "<<tmp[2]<<"\n";

        rotateCloud<<<blocksPerGrid,THREADS_PER_BLOCK>>>(dev_p1, dev_R, dev_p1_size);

//        cudaMemcpy(tmp, dev_p1, 3*sizeof(float), cudaMemcpyDeviceToHost);
//        std::cout<<"p1 after rotation "<<tmp[0]<<" "<<tmp[1]<<" "<<tmp[2]<<"\n";

        cudaEventRecord(stopApplyingRotation, 0);
        cudaEventSynchronize(stopApplyingRotation);
        cudaEventElapsedTime(&elapsedTimeApplyingRotation, startApplyingRotation, stopApplyingRotation);
        cudaEventDestroy(startApplyingRotation);
        cudaEventDestroy(stopApplyingRotation);

        if(isShowDebugData) {
            std::cout<<"Rotation time "<<elapsedTimeApplyingRotation<<"\n";
        }

        //---------------------------------Вычисление центроида первого облака

        //Отметка времени
        cudaEventCreate(&startCentroidCalc);
        cudaEventCreate(&stopCentroidCalc);
        cudaEventRecord(startCentroidCalc, 0);
        //Первый метод
        //centroidCalc<<<blocksPerGrid,THREADS_PER_BLOCK>>>(dev_p1, dev_mean_1, 1, dev_npIndex, dev_NPnumber, dev_p2_size);
        //Второй метод
        centroidsDualCalc<<<blocksPerGrid,THREADS_PER_BLOCK>>>(dev_p1_size, dev_p2_size, dev_p1, dev_p2, dev_npIndex, dev_NPnumber, dev_mean_1, dev_mean_2);



        //В результате вычисление мы имеем вектор, который нужно окончательно просуммировать
        //Чтобы не копировать данные в память CPU, сделаем это в одном потоке на GPU
        //Первый метод
        //centrFinalCalc<<<1,1>>>(dev_mean_1, blocksPerGrid, dev_NPnumber);
        //Второй метод
        centrDualFinalCalc<<<1,1>>>(dev_mean_1, dev_mean_2, dev_NPnumber, dev_p2_size);

        cudaEventRecord(stopCentroidCalc, 0);
        cudaEventSynchronize(stopCentroidCalc);
        cudaEventElapsedTime(&elapsedTimeCentroidCalc, startCentroidCalc, stopCentroidCalc);
        cudaEventDestroy(startCentroidCalc);
        cudaEventDestroy(stopCentroidCalc);

        if(isShowDebugData) {
            std::cout<<"Centroid calculation time "<<elapsedTimeCentroidCalc<<"\n";
        }




        //--------------------------------Применение перемещения
        //Отметка времени
        cudaEventCreate(&startApplyTranslation);
        cudaEventCreate(&stopApplyTranslation);
        cudaEventRecord(startApplyTranslation, 0);

        applyTranslation<<<blocksPerGrid, THREADS_PER_BLOCK>>>(dev_p1, dev_mean_1, dev_mean_2, dev_p1_size, dev_finalTransformation);

        //После применения перемещений отправляем полученные значения в вектор трансформации
        cudaMemcpy(tmp_transformation, dev_finalTransformation, 6*sizeof(float), cudaMemcpyDeviceToHost);
        //Затем суммируем перемещение
        finalTransformation[0] = finalTransformation[0] + tmp_transformation[0];
        finalTransformation[1] = finalTransformation[1] + tmp_transformation[1];
        finalTransformation[2] = finalTransformation[2] + tmp_transformation[2];

        //Финальная отметка времени
        cudaEventRecord(stopApplyTranslation, 0);
        cudaEventSynchronize(stopApplyTranslation);
        cudaEventElapsedTime(&elapsedTimeApplyTranslation, startApplyTranslation, stopApplyTranslation);
        cudaEventDestroy(startApplyTranslation);
        cudaEventDestroy(stopApplyTranslation);

        if(isShowDebugData) {
            std::cout<<"Applying translation time "<<elapsedTimeApplyTranslation<<"\n";
        }


    }

    //Получение окончательного преобразования
    //Из кватернионов в матрицу поврота
    Eigen::Matrix3f R_t2 = QfinalTransf->toRotationMatrix();

    //Из матрицы поворота находим углы Эйлера
    finalTransformation[4] = asin(-R_t2(2,0));
    finalTransformation[3] = asin( R_t2(2,1) / cos(finalTransformation[4]));
    finalTransformation[5] = asin (R_t2(1,0) / cos(finalTransformation[4]));

    //! Этап трансформации - не является обязательным и требует написания дополнительной функции
    //Копируем преобразованное облако с памяти устройства на память хоста
    //cudaMemcpy(host_p1, dev_p1, *host_p1_size*POINT_DIM* sizeof(float), cudaMemcpyDeviceToHost);

    //Очищение памяти
    free(tmp_transformation);
    free(tmp_rotMatrix);
    free(tmp_transformation2);

//    free(host_mean_p1);
//    free(host_mean_p2);

    if(isShowDebugData) {
        //Отображаем значение окончательного преобразования
        std::cout<<"Final translation "<<finalTransformation[0]<<" "<<finalTransformation[1]<<" "<<finalTransformation[2]<<"\n";
        std::cout<<"Final rotation "<<finalTransformation[3]<<" "<<finalTransformation[4]<<" "<<finalTransformation[5]<<"\n";
        std::cout<<"//---------------------------------End---------------------------------//";
    }

}


