#include <ros/ros.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <unistd.h>


#include <fstream>
#include <sys/types.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <iomanip>

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include "sensor_msgs/ChannelFloat32.h"
#include "arcada_velodyne_grabber/VelodyneImu.h"

#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925

//-------------------------УКАЗАТЕЛИ НА ПЕРЕМЕННЫЕ ХРАНЕНИЯ ДАННЫХ
//Массив положений головки
static float *rotHeadPositionArray;
//Массив данных с лазеров
static float *laserData;
//Массив данных интенсивности
static int *intensityData;
//Количество пакетов данных
static int dataPacketsNmb;
//--------------------------------------------------------------------------------------

//------------------------ФЛАГИ---------------------------------------------------------
//Флаг на разрешение обработки данных
static bool isAllowToProcess;
static bool isProcessingPerforming;
//--------------------------------------------------------------------------------------

//-----------------------Положения лазеров------
static const int laserOrder[] = {0, 16, 1, 17, 2, 18, 3, 19, 4, 20, 5, 21, 6, 22, 7, 23, 8, 24, 9, 25, 10, 26, 11, 27, 12, 28, 13, 29, 14, 30, 15, 31};
static const float vertAngles[] = {-30.67, -29.33, -28.00, -26.66, -25.33, -24.00, -22.67, -21.33, -20.00, -18.67,-17.33, -16.00, -14.67, -13.33, -12.00, -10.67, -9.33, -8.00, -6.66, -5.33, -4.00, -2.67, -1.33, 0, 1.33, 2.67, 4.00, 5.33, 6.67, 8.00, 9.33, 10.67};
//-----------------------------------------------



using namespace std;

struct Velodyne_GYRO {

    //Гироскоп
    int gyro1;
    int gyro2;
    int gyro3;

    //Акселлерометр
    int accel1X;
    int accel2X;
    int accel3X;

    int accel1Y;
    int accel2Y;
    int accel3Y;

    //Температура
    int temp1;
    int temp2;
    int temp3;
};

//Функция окончания работы
void mySigintHandler(int sig) {

    //Ждём, пока не снимется флаг с обработки
    while(isProcessingPerforming) {
       usleep(1);
    }
    isAllowToProcess = false;

    //Очищаем память
    free(laserData);
    free(intensityData);
    free(rotHeadPositionArray);

    ROS_INFO("VELODYNE GRABBER stopped");

  ros::shutdown();
}



//Функция формирования посылки
void formPointCloudMessage(sensor_msgs::PointCloud *pointCloud) {

    //Очищаем облако точек
    pointCloud->points.clear();
    pointCloud->channels.clear();

    //Выстраивание лазеров в нужной последовательности
    //Временный массив лазеров
    float tmp_laserData[LASER_NMB];
    //Временный массив интенсивности
    int tmp_intensityData[LASER_NMB];
    //Проход по всем пакетам
    for (int i = 0; i < dataPacketsNmb; i++) {
        //Проход по всем лазерам и перевод значений дальности и интенсивности во временные массивы
        //Кроме этого, не забываем корректировать в метры (1 единица дальности = 2 мм)
        for (int j = 0; j < LASER_NMB; j++) {
            //Перевод дальности
            tmp_laserData[laserOrder[j]] = laserData[i*LASER_NMB + j]/500;
            //Вывод значения
            //qDebug()<<"Laser "<<j<<" "<<RAW_Laser_Full->laserRange[i*LASER_NMB + j];
            //Перевод интенсивности
            tmp_intensityData[laserOrder[j]] = intensityData[i*LASER_NMB + j];
        }

        //Возвращаем значения обратно
        for (int j = 0; j < LASER_NMB; j++) {
            laserData[i*LASER_NMB + j] = tmp_laserData[j];
            intensityData[i*LASER_NMB + j] = tmp_intensityData[j];
        }
    }

    //Интенсивность отражения точки
    sensor_msgs::ChannelFloat32 intensity;
    pointCloud->channels.resize(1);
    pointCloud->channels[0].name = "intensity";
    pointCloud->channels[0].values.resize(LASER_NMB*dataPacketsNmb);

    //Проходимся по всем пакетам и записываем их в очерёдности
    for(int packet = 0; packet < dataPacketsNmb; packet++) {
        for(int laser = 0; laser < LASER_NMB; laser++) {
            //Вычисляем расстояние xy
            float xydistance = (laserData[packet*LASER_NMB + laser]) * cos(RAD_PER_GRAD * vertAngles[laser]);
            //Внедряемая точка
            geometry_msgs::Point32 point;
            //Вычисляем координату x
            point.x = xydistance * sin((RAD_PER_GRAD * rotHeadPositionArray[packet])/100 + M_PI/2);
            //Вычисляем координату y
            point.y = xydistance * cos((RAD_PER_GRAD * rotHeadPositionArray[packet])/100 + M_PI/2);
            //Вычисляем координату z
            //Для машины добавляем 2,4, для KUKA 0.55, для тумбочки 1,1
            point.z = laserData[packet*LASER_NMB + laser] * sin(RAD_PER_GRAD * vertAngles[laser]);
            //Выводим точку в облако
            pointCloud->points.push_back(point);

            //Интенсивность отражения точки
            pointCloud->channels[0].values[packet*LASER_NMB + laser] = intensityData[packet*LASER_NMB + laser]%4096;

//            cout<<intensityData[packet*LASER_NMB + laser]<<endl;
        }
    }
}


int main(int argc, char **argv) {

    //Начальная инициализация узла
    ros::init(argc, argv, "arcada_velodyne_grabber_node");
    ros::NodeHandle n;
    std::string frame_id;
    n.param<std::string> ("frame_id", frame_id, "/arcada_pointCloud");

    //Публикатор облака точек
    ros::Publisher pubCloud = n.advertise<sensor_msgs::PointCloud>("arcada/velodyne_pointCloud", 2);

    //Публикатор данных гироскопа
    ros::Publisher gyroVelPub = n.advertise<arcada_velodyne_grabber::VelodyneImu>("arcada/velodyne_imu",2);

    sensor_msgs::PointCloud pointCloud;

    //Сообщение о запуске
    ROS_INFO("VELODYNE GRABBER started");

    //Функция при завершении работы
    signal(SIGINT, mySigintHandler);

    //Частота работы
    ros::Rate rate(30000); //Hz

    //------------------------------ФЛАГИ-----------------------------------------------------
    //Флаг захвата из предзаписанного файла
    bool isGrabbingDataFromFile = false;
    //Флаг установления соединения
    bool EstablishConnectionFailed;
    //----------------------------------------------------------------------------------------

    //------------------------------СЧЁТЧИКИ--------------------------------------------------
    unsigned int pkt_counter_laser=0;
    unsigned int pkt_counter_gyro = 0;
    //----------------------------------------------------------------------------------------

    //-----------------------------ПЕРЕМЕННЫЕ ДЛЯ ЗАХВАТА-------------------------------------
    //Заголовок файла
    struct pcap_pkthdr header;
    //Буффер данных пакета лидар
    const u_char *packet;
    //Обработчик лидара
    pcap_t *handle;
    //Структура данных с гироскопа
    arcada_velodyne_grabber::VelodyneImu VG;
    //----------------------------------------------------------------------------------------

    //------------------------------ВЫДЕЛЕНИЕ ПАМЯТИ НА ХРАНЕНИЕ ДАННЫХ-----------------------
    rotHeadPositionArray = (float *) malloc(MAX_BLOCKS_PER_ROT * sizeof(float));
    dataPacketsNmb = 0;
    laserData = (float *) malloc(MAX_BLOCKS_PER_ROT * LASER_NMB * sizeof(float));
    intensityData = (int *) malloc(MAX_BLOCKS_PER_ROT * LASER_NMB * sizeof(int));
    //------------------------------УСТАНОВКА СОЕДИНЕНИЯ---------------------------------------

    //----Режим захвата из файла-----
    if(isGrabbingDataFromFile) {


        //Выражение фильтра
        char filter_app[] = "udp";

        //Выражение фильтрации в составленном виде
        //Лидар
        struct bpf_program fp;

        //Маска подсети
        bpf_u_int32 mask = 0xFFFFFF00;

        //Заполнение массива пути файла считывания
        string filePath = "/home/ls/VelodyneCapture/LAB33-1.pcap";
        char *fileName = new char[filePath.size()+1];
        fileName[filePath.size()]=0;
        memcpy(fileName,filePath.c_str(),filePath.size());

        //Открываем PCAP файл для считывания


        //Буффер ошибок
        char errbuf[PCAP_ERRBUF_SIZE];

        //Открваем выбранный файл
        handle = pcap_open_offline(fileName, errbuf);

        //В случае, если произошла неудача
        if (handle == NULL) {
            ROS_INFO("Can't connect to grabber file");
            //Устанавливаем флаг
            EstablishConnectionFailed = true;
        }


        //Если не было ошибки
        if (!EstablishConnectionFailed) {

            //Применение фильтра
            //В случае, если неудача, то выводим ошибку
            if (pcap_compile(handle, &fp, filter_app, 0, mask) == -1) {
                ROS_INFO("Lidar Filter error");
            }
        }


        //Если не было ошибки
        if (!EstablishConnectionFailed) {

            //Загрузка имеющегося фильтра
            if (pcap_setfilter(handle, &fp) == -1) {
                fprintf(stderr, "%s", pcap_geterr(handle));

            }
            //Устанавливаем разрешение на обработку
            isAllowToProcess = true;
        }
    }
    else {


        ROS_INFO("Setting parameters");
        //Выражение фильтра
        //Передача данных по протоколу UDP
        char filter_app[] = "udp";

        //Выражение фильтрации в составленном виде
        //Лидар
        struct bpf_program fp;

        ROS_INFO("IP and NETMASK");
        //Маска подсети
        bpf_u_int32 mask = 0xFFFFFF00;
        //Ip адрес 192.168.1.30
        bpf_u_int32 net = 0xC0A801C9;


        //Устройство
        char *dev;
        //Буффер ошибок
        char errbuf[PCAP_ERRBUF_SIZE];


        //Определение устройства
        dev = pcap_lookupdev(errbuf);
        if (dev == NULL ) {

            //Установка флага
            EstablishConnectionFailed = true;
            ROS_INFO("Failed to install connection - device");
        }

        if (!EstablishConnectionFailed) {
            //Поиск свойств устройства
            if(pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {

                //Установка флага
                EstablishConnectionFailed = true;
                ROS_INFO("Failed to install connection");
            }
        }




        //Открываем для живого захвата
        handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

        if (!EstablishConnectionFailed) {
            //В случае, если произошла неудача
            if (handle == NULL) {

                //Установка флага
                EstablishConnectionFailed = true;
                ROS_INFO("Failed to install connection");
            }
        }

        if (!EstablishConnectionFailed) {
            //Применение фильтра
            //В случае, если неудача, то выводим ошибку
            if (pcap_compile(handle, &fp, filter_app, 0, mask) == -1) {

                //Установка флага
                EstablishConnectionFailed = true;
                ROS_INFO("Failed to install connection");
            }
        }

        if (!EstablishConnectionFailed) {
            //Загрузка имеющегося фильтра
            if (pcap_setfilter(handle, &fp) == -1) {
                fprintf(stderr, "%s", pcap_geterr(handle));

                //Установка флага
                EstablishConnectionFailed = true;
                ROS_INFO("Failed to install connection");
            }
        }
        //Устанавливаем разрешение на обработку
        isAllowToProcess = true;
    }
    //-----------------------------------------------------------------------------------------

    while(ros::ok()) {

        //Основной цикл алгоритма
        //Если есть разрешение
        //Пока имеются пакеты в файле
        if((packet = pcap_next(handle,&header)) && isAllowToProcess && !EstablishConnectionFailed) {


            //Распределяем пакеты
            if(packet[34] == 0x09 && packet[35] == 0x40) {
                //Значит данные пришли с лидара
                //Парсинг данных с лазеров
                //Пропускаем заголовок Ethrnet и начинаем обрабатывать двенадцать пакетов
                //Ставим флаг выполнения обработки
                isProcessingPerforming = true;

                for (int i = 42; i < 1242; i=i+100) {
                    //qDebug()<<"Packet data "<<i<<" "<<packet[i]<<" "<<(packet[i] & 0x8);

                    //Проверяем стартовый идентификатор 0xEEFF
                    if (packet[i] == 0xFF && packet[i+1] == 0xEE) {
                        //qDebug()<<"Found start identifier "<<i;

                        //Вычисляем угловую позицию головы (2 байта)
                        float anglePosition = (packet[i+2] + packet[i+3] * 256);
                        //В случае пересечени головки нулевой отметки формируем облако
                        if ((anglePosition - rotHeadPositionArray[dataPacketsNmb - 1]) < 0 && pkt_counter_laser > 2) {
//                            ROS_INFO("//-------------------------FORM CLOUD--------------------------------------//");



                            //Здесь нужно отправить посылку о готовности данных или отправить сами данные
                            //Отправляем данные на обработку
                            formPointCloudMessage(&pointCloud);
                            //Заполняем заголовочные данные
                            pointCloud.header.frame_id = frame_id;
                            pointCloud.header.stamp = ros::Time::now();
                            //Публикация
                            pubCloud.publish(pointCloud);
//                            cout<<"Number of points "<<pointCloud.channels;

                            //Обнуление счётчкика пакетов
                            dataPacketsNmb = 0;

                        }
                        rotHeadPositionArray[dataPacketsNmb] = anglePosition;
                        //qDebug()<<"Rotation position "<<RAW_Laser->rotationPos[*RAW_Laser->packetNmb];

                        //Вычисляем значения по лазерам
                        for (int j = 4; j < 100; j = j+3) {
                            //Текущее смещение в пакете
                            int  curOffset = i+j;
                            //Номер лазера
                            int laserNmb = (j-4)/3;
                            //Вычисляем расстояние
                            laserData[dataPacketsNmb*LASER_NMB + laserNmb] = packet[curOffset] + packet[curOffset + 1] * 256;
                            //Вычисляем интенсивность
                            intensityData[dataPacketsNmb*LASER_NMB + laserNmb] = packet[curOffset + 2];
                            //Выводим
                            //qDebug()<<"Laser "<<((*RAW_Laser->packetNmb)*LASER_NMB + laserNmb)<<" distance "<<(RAW_Laser->laserRange[*RAW_Laser->packetNmb + laserNmb]);
                        }
                    }
                    dataPacketsNmb++;
                }

                pkt_counter_laser++; //increment number of packets seen
                //Снимаем флаг выполнения обработки
                isProcessingPerforming = false;
            }
            //                     qDebug()<<"Packets from HDL number "<<*RAW_Laser->packetNmb;
            if(packet[34] == 0x20 && packet[35] == 0x74) {
                //Значит данные пришли с гироскопа
                //Инкрементируем количество пакетов с гироскопа
                pkt_counter_gyro++;

                //Парсинг данных
                //Сначала данные с гироскопа
                //Gyro 1
                int startPackNumb = 56;
                VG.gyro1 =  (packet[startPackNumb] + (packet[startPackNumb+1] & 0x0F) * 256 - (0x8 & packet[startPackNumb+1]) * 512) * 0.09766;
                VG.temp1 =  (packet[startPackNumb+2] + (packet[startPackNumb+3] & 0x0F) * 256 - (0x8 & packet[startPackNumb+3]) * 512) * 0.1453 + 25;
                VG.accel1X = (packet[startPackNumb+4] + (packet[startPackNumb+5] & 0x0F) * 256 - (0x8 & packet[startPackNumb+5]) * 512) * 0.001221;
                VG.accel1Y = (packet[startPackNumb+6] + (packet[startPackNumb+7] & 0x0F) * 256 - (0x8 & packet[startPackNumb+7]) * 512) * 0.001221;
                //Gyro 2
                VG.gyro2 =  (packet[startPackNumb+8] + (packet[startPackNumb+9] & 0x0F) * 256 - (0x8 & packet[startPackNumb+9]) * 512) * 0.09766;
                VG.temp2 =  (packet[startPackNumb+10] + (packet[startPackNumb+11] & 0x0F) * 256 - (0x8 & packet[startPackNumb+11]) * 512) * 0.1453 + 25;
                VG.accel2X = (packet[startPackNumb+12] + (packet[startPackNumb+13] & 0x0F) * 256 - (0x8 & packet[startPackNumb+13]) * 512) * 0.001221;
                VG.accel2Y = (packet[startPackNumb+14] + (packet[startPackNumb+15] & 0x0F) * 256 - (0x8 & packet[startPackNumb+15]) * 512) * 0.001221;
                //Gyro 3
                VG.gyro3 =  (packet[startPackNumb+16] + (packet[startPackNumb+17] & 0x0F) * 256 - (0x8 & packet[startPackNumb+17]) * 512) * 0.09766;
                VG.temp3 =  (packet[startPackNumb+18] + (packet[startPackNumb+19] & 0x0F) * 256 - (0x8 & packet[startPackNumb+19]) * 512) * 0.1453 + 25;
                VG.accel3X = (packet[startPackNumb+20] + (packet[startPackNumb+21] & 0x0F) * 256 - (0x8 & packet[startPackNumb+21]) * 512) * 0.001221;
                VG.accel3Y = (packet[startPackNumb+22] + (packet[startPackNumb+23] & 0x0F) * 256 - (0x8 & packet[startPackNumb+23]) * 512) * 0.001221;

                //Публикуем
                gyroVelPub.publish(VG);

            }

//        qDebug()<<"PCAP process finished";
//        //Закрываем файл
//        pcap_close(handle);
//    }
//    //Если всё таки произошла ошабка
//    else {
//        //Выдаём сигнал об ошибке
//        lidarConnectionError();
//    }

//    //Закрываем файл для записи данных гироскопа
//    fileToWrite.close();


        }
        ros::spinOnce();
        rate.sleep();
    }


    ROS_INFO("VELODYNE GRABBER finished");
    return 0;

}
