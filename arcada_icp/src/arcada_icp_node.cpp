#include <ros/ros.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include <fstream>
#include <sys/types.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <iomanip>

#include <cuda.h>
#include <cuda_runtime.h>

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include "sensor_msgs/ChannelFloat32.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Path.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseStamped.h"

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <tf/tfMessage.h>
//#include <eigen3/Eigen/StdVector>
//#include <eigen3/Eigen/Dense>
//#include <eigen3/Eigen/Geometry>
//#include <eigen3/Eigen/LU>

#include "boost/date_time/posix_time/posix_time.hpp"

#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3
#define THREADS_PER_BLOCK 1024

sensor_msgs::PointCloud pointCloudMain;

using namespace std;

//--------------------УКАЗАТЕЛИ НА МАССИВЫ ДАННЫХ------------
static float *dev_pointCloud;
static int *dev_size;
static int *dev_intensity;
static float *host_pointCloud;
static int *host_size;
static int *host_intensity;

static float *dev_pointCloudPrev;
static int *dev_sizePrev;
static int *dev_intensityPrev;
static float *host_pointCloudPrev;
static int *host_sizePrev;
static int *host_intensityPrev;

//Указатели на массивы облака точек, относящиеся к земле
static float *dev_pointCloudGroundPrev;
static int *dev_sizeGroundPrev;
static int *dev_intensityGroundPrev;
static float *host_pointCloudGroundPrev;
static int *host_sizeGroundPrev;
static int *host_intensityGroundPrev;
static float *dev_pointCloudGround;
static int *dev_sizeGround;
static int *dev_intensityGround;
static float *host_pointCloudGround;
static int *host_sizeGround;
static int *host_intensityGround;

//Указатели на GPU массивы и переменные
//Выделение памяти для массива соответствий
int *dev_npIndex;
//Выделение памяти для масива расстояний ближайших точек
float *dev_mindist;
float *dev_mean_dist;
//Выделение памяти для цетроидов двух облаков
//В зависимости от этого - выделение памяти для центроидов
float *dev_mean_1;
float *dev_mean_2;
//Выделение памяти под матрицу для сингулярного разложения
float *dev_M2;
//Выделение памяти под матрицу поворота
float *dev_R;
//Выделение памяти под среднее значение расстояния
float *dev_averDist;
//Количество итераций алгоритма ICP.
int iteration_ICP;
//Выделение памяти под массив редукции
int *dev_NPnumber;
//Выделение памяти под облако нормалей
float *dev_normalsCloud_2;

int *npIndex;
int x_min, x_max, y_min, y_max, z_min, z_max;
int segInterval;
int dim_x, dim_y, dim_z;
int segmentSize;

int *p1_seg, *p2_seg;

int *dev_p1_seg, *dev_p2_seg;


//Массив параметров итоговых значений преобразования
float *finalTransformation;
//И он же на устройстве
float *dev_finalTransformation;

//Кватернион итогового преобразования
Eigen::Quaternionf *QfinalTransf;
//Матрица поворота итогового преобразования
Eigen::Matrix3f *FinalRotationMat;

static const int blocksPerGrid = ((N_MAX + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK);
//----------------------------------------------------------

//------------------------ФЛАГИ---------------------------------------------------------
//Флаг на разрешение обработки данных
static bool isAllowToProcess;
//Флаг, сообщающий, что в настоящее время выполняется обработка
static bool isProcessingPerforming;
//Флаг получения облака точек
static bool isReceivedCloud;
//Флаг успешного сопоставления облаков точек
static bool isAlignementSuccesfull;
//Флаг готовности предыдущего облака точек
static bool isPreviousCloudReady;
//Флаг готовности к получению облака точек
static bool isReadytoNewCloud;
//--------------------------------------------------------------------------------------

//-------------------------СЧЁТЧИКИ-----------------------------------------------------
//Количество полученных облаков
int numberOfReceivedCloud;
//Количество обработанных облаков
int numberOfProceedClouds;
//--------------------------------------------------------------------------------------

//Объявление функции анализа проходимости на видеокарте
void filterCloud_CUDA(float *dev_pointCloud,
                      int *host_p_size,
                      int *dev_p_size,
                      int *dev_intensity,
                      float rtp_height,
                      float rtp_forward,
                      float rtp_backward,
                      float rtp_side,
                      float lowLevel,
                      float hightLevel,
                      float maxRange,
                      float initX,
                      float initY);

void kernelICP(float *dev_p1,
               float *dev_p2,
               int *dev_p1_size,
               int *dev_p2_size,
               int *host_p1_size,
               int *host_p2_size,
               int *dev_npIndex,
               float *dev_mean_1,
               float *dev_mean_2,
               float *dev_M2,
               float *dev_R,
               int iteration_ICP,
               int *p1_seg,
               int dim_x,
               int dim_y,
               int dim_z,
               int segmentSize,
               int blocksPerGrid,
               int *dev_NPnumber,
               float *finalTransformation,
               float *dev_finalTransformation,
               Eigen::Quaternionf *QfinalTransf,
               float *dev_mindist,
               int *dev_intensity,
               int *dev_intensityPrev);

//Функция принятия облака точек
void pointCloud_callback(const sensor_msgs::PointCloud &pointCloud) {

    if((isAllowToProcess && isReadytoNewCloud) || numberOfProceedClouds == 0) {

        isProcessingPerforming = true;
        ROS_INFO("Receive cloud");
        //Инкрементируем количество полученных облаков
        numberOfReceivedCloud++;

        ROS_INFO("TEST");

        //Сначала переносим текущее облако в предыдущее в случае, если произошло успешное сопоставление
        if((isAlignementSuccesfull || numberOfProceedClouds == 0) && numberOfReceivedCloud > 1) {
            for(int i = 0; i < *host_size; i++) {
                //Сначала координаты точек
                host_pointCloudPrev[i * POINTS_DIM + 0] = host_pointCloud[i * POINTS_DIM + 0];
                host_pointCloudPrev[i * POINTS_DIM + 1] = host_pointCloud[i * POINTS_DIM + 1];
                host_pointCloudPrev[i * POINTS_DIM + 2] = host_pointCloud[i * POINTS_DIM + 2];
                //Потом интенсовность
                host_intensityPrev[i] = host_intensity[i];
            }
            *host_sizePrev = *host_size;
            //Копируем всё на память устройства
            cudaMemcpy(dev_pointCloudPrev, host_pointCloudPrev, *host_sizePrev * POINTS_DIM * sizeof(float), cudaMemcpyHostToDevice);
            cudaMemcpy(dev_intensityPrev, host_intensityPrev, *host_sizePrev * sizeof(int), cudaMemcpyHostToDevice);
            cudaMemcpy(dev_sizePrev, host_sizePrev, sizeof(int), cudaMemcpyHostToDevice);
            //Устанавливаем флаг готовности предыдущего облака точек
            isPreviousCloudReady = true;
        }
        else if(numberOfReceivedCloud > 1) {
            //Устанавливаем флаг готовности предыдущего облака точек
            isPreviousCloudReady = true;
        }


        //Далее, формируем новое
        *host_size = pointCloud.points.size();
        //Переносим все параметры
        for(int i = 0; i < pointCloud.points.size(); i++) {
            host_pointCloud[POINT_DIM * i + 0] = pointCloud.points[i].x;
            host_pointCloud[POINT_DIM * i + 1] = pointCloud.points[i].y;
            host_pointCloud[POINT_DIM * i + 2] = pointCloud.points[i].z;
            //Интенсивность
            host_intensity[i] = pointCloud.channels[0].values[i];
        }
        //Копируем на память устройства
        cudaMemcpy(dev_pointCloud, host_pointCloud, *host_size * POINTS_DIM * sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(dev_intensity, host_intensity, *host_size * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dev_size, host_size, sizeof(int), cudaMemcpyHostToDevice);
        isReceivedCloud = true;
        isProcessingPerforming = false;
        isReadytoNewCloud = false;

        //Облако точек для отправки
        //Публикация трансформированного облака точек
        //Формируем облако точек
        //Очищаем облако точек
        pointCloudMain.points.clear();
        pointCloudMain.channels.clear();
        pointCloudMain.channels.resize(1);
        pointCloudMain.channels[0].name = "intensity";
        pointCloudMain.channels[0].values.resize(*host_size);

        //Проходимся по всем пакетам и записываем их в очерёдности
        for(int i = 0; i< *host_size; i++) {
                geometry_msgs::Point32 point;
                point.x = host_pointCloud[i*POINTS_DIM + 0];
                point.y = host_pointCloud[i*POINTS_DIM + 1];
                point.z = host_pointCloud[i*POINTS_DIM+ 2];
                //Выводим точку в облако
                pointCloudMain.points.push_back(point);

                //Интенсивность отражения точки
                pointCloudMain.channels[0].values[i] = host_intensity[i];

        }
    }


}

//Функция окончания работы
void mySigintHandler(int sig) {

    ROS_INFO("FINISHING...");

    //Ждём, пока не снимется флаг с обработки
//    while(isProcessingPerforming) {
//       usleep(100);
//    }
    isAllowToProcess = false;

    usleep(1000);

    //Освобождаем память
    cudaFree(dev_pointCloud);
    cudaFree(dev_size);
    cudaFree(dev_intensity);

    free(host_pointCloud);
    free(host_intensity);
    free(host_size);

    cudaFree(dev_pointCloudPrev);
    cudaFree(dev_sizePrev);
    cudaFree(dev_intensityPrev);

    free(host_pointCloudPrev);
    free(host_intensityPrev);
    free(host_sizePrev);

    cudaFree(dev_pointCloudGround);
    cudaFree(dev_sizeGround);
    cudaFree(dev_intensityGround);

    free(host_pointCloudGround);
    free(host_intensityGround);
    free(host_sizeGround);

    cudaFree(dev_pointCloudGroundPrev);
    cudaFree(dev_sizeGroundPrev);
    cudaFree(dev_intensityGroundPrev);

    free(host_pointCloudGroundPrev);
    free(host_intensityGroundPrev);
    free(host_sizeGroundPrev);

    cudaFree(dev_npIndex);
    cudaFree(dev_mindist);
    cudaFree(dev_mean_dist);
    cudaFree(dev_mean_1);
    cudaFree(dev_mean_2);
    cudaFree(dev_M2);
    cudaFree(dev_R);
    cudaFree(dev_averDist);
    cudaFree(dev_NPnumber);
    cudaFree(dev_normalsCloud_2);
    cudaFree(dev_finalTransformation);
    free(finalTransformation);
    free(QfinalTransf);
    free(npIndex);
    free(p1_seg);
    free(p2_seg);
    cudaFree(dev_p1_seg);
    cudaFree(dev_p2_seg);


    ROS_INFO("ARCADA ICP stopped");

  ros::shutdown();
}



int main(int argc, char **argv) {

    //Начальная инициализация узла
    ros::init(argc, argv, "arcada_cloud_filter_node");
    ros::NodeHandle n;
    std::string frame_id;
    n.param<std::string> ("frame_id", frame_id, "/arcada_pointCloud");

    //-----------------------УКАЗАТЕЛИ НА ПЕРЕСЫЛАЕМЫЕ И ПРИНИМАЕМЫЕ ДАННЫЕ----------
    nav_msgs::OccupancyGrid OccupancyMap;
    //Подписчик на облако точек
    ros::Subscriber pointCloud_sub = n.subscribe("arcada/velodyne_pointCloud",32, pointCloud_callback);
    //Публикатор траектории и трансформации
    nav_msgs::Path SlamTraj;
    ros::Publisher TrajPub = n.advertise<nav_msgs::Path>("arcada/slam_traj",4);

    //Публикатор трансформаций
    ros::Publisher transfPub;
    transfPub = n.advertise<tf::tfMessage> ("/tf", 32);

    //Публикатор трансформированного облака точек
    ros::Publisher pubCloud = n.advertise<sensor_msgs::PointCloud>("arcada/velodyne_pointCloud", 2);
    //-------------------------------------------------------------------------------

    //-----------------------ВЫДЕЛЕНИЕ ПАМЯТИ ПОД МАССИВЫ ДАННЫХ--------------------
    cudaMalloc((void**)&dev_pointCloud, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_intensity, N_MAX*sizeof(int));
    cudaMalloc((void**)&dev_size, sizeof(int));

    host_pointCloud = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_intensity = (int *) malloc(N_MAX * sizeof(int));
    host_size = (int *) malloc(sizeof(int));

    cudaMalloc((void**)&dev_pointCloudPrev, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_intensityPrev, N_MAX*sizeof(int));
    cudaMalloc((void**)&dev_sizePrev, sizeof(int));

    host_pointCloudPrev = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_intensityPrev = (int *) malloc(N_MAX * sizeof(int));
    host_sizePrev = (int *) malloc(sizeof(int));


    cudaMalloc((void**)&dev_pointCloudGround, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_intensityGround, N_MAX*sizeof(int));
    cudaMalloc((void**)&dev_sizeGround, sizeof(int));

    host_pointCloud = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_intensity = (int *) malloc(N_MAX * sizeof(int));
    host_size = (int *) malloc(sizeof(int));

    cudaMalloc((void**)&dev_pointCloudGroundPrev, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_intensityGroundPrev, N_MAX*sizeof(int));
    cudaMalloc((void**)&dev_sizeGroundPrev, sizeof(int));

    host_pointCloudGroundPrev = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_intensityGroundPrev = (int *) malloc(N_MAX * sizeof(int));
    host_sizeGroundPrev = (int *) malloc(sizeof(int));

    //Указатели на GPU массивы и переменные

    //Выделение памяти для массива соответствий
    cudaMalloc((void**)&dev_npIndex, N_MAX * sizeof(int));

    //Выделение памяти под расстояния до ближайших точек
    cudaMalloc((void**)&dev_mindist, N_MAX * sizeof(float));
    cudaMalloc((void**)&dev_mean_dist, N_MAX * sizeof(float));

    //Выделение памяти для цетроидов двух облаков
    //Определение количества блоков для задания длины вектора

    //В зависимости от этого - выделение памяти для центроидов
    cudaMalloc((void**)&dev_mean_1, POINT_DIM*blocksPerGrid*sizeof(float));
    cudaMalloc((void**)&dev_mean_2, POINT_DIM*blocksPerGrid*sizeof(float));

    //Выделение памяти под облако нормалей
    cudaMalloc((void**)&dev_normalsCloud_2, POINT_DIM*N_MAX*sizeof(float));

    //Выделение памяти под матрицу для сингулярного разложения
    cudaMalloc((void**)&dev_M2, POINT_DIM*POINT_DIM*blocksPerGrid*sizeof(float));
    cudaMemset(dev_M2, 0, POINT_DIM*POINT_DIM*blocksPerGrid*sizeof(float));

    //Выделение памяти под матрицу поворота
    cudaMalloc((void**)&dev_R, POINT_DIM*POINT_DIM*sizeof(float));

    //Выделение памяти под среднее значение расстояния
    cudaMalloc((void**)&dev_averDist, N_MAX*sizeof(float));

    //Количество итераций алгоритма ICP.
    //По умолчанию 40 - оптимальный вариант
    iteration_ICP = 60;

    //Выделение памяти под массив редукции
    cudaMalloc((void**)&dev_NPnumber, blocksPerGrid*sizeof(int));

    //Выделение памяти для вектора итоговых значений
    //На хосте
    finalTransformation = (float *) malloc(6 *sizeof(float));
    //На устройстве
    cudaMalloc((void**)&dev_finalTransformation, 6*sizeof(float));

    //Выделяем память под кватернион итогового преобразования
    QfinalTransf = (Eigen::Quaternionf *) malloc(sizeof(Eigen::Quaternionf));

    //Массив индексов ближайших точек
    npIndex = (int *) malloc(N_MAX *sizeof(int));
    //Пространство разбивается следующим образом:
    //по x от -100 до 100
    x_min = -100;
    x_max = 100;
    //по y от -100 до 100
    y_min = -100;
    y_max = 100;
    //по z от -50 до 50
    z_min = -100;
    z_max = 100;
    //Интервал разбиения
    segInterval = 10;
    //Размерность
    //по x
    dim_x = (x_max - x_min + segInterval - 1)/segInterval;
    //по y
    dim_y = (y_max - y_min + segInterval - 1)/segInterval;
    //по z
    dim_z = (z_max - z_min + segInterval - 1)/segInterval;

    segmentSize = 500;
    //Для первого облака выделяем массив, состоящий из dim_x*dim_y*dim_z подмассивов с размерностью segmentSize
    p1_seg    = (int *) malloc(dim_x*dim_y*dim_z*segmentSize * sizeof(int));
    //Для второго выделяем массив из p2_nb элементов
    p2_seg  = (int *) malloc(N_MAX * sizeof(int));

    //Выделение памяти под массив сегментации на устройстве
    cudaMalloc((void**)&dev_p1_seg, dim_x*dim_y*dim_z* segmentSize *sizeof(int));
    cudaMalloc((void**)&dev_p2_seg, N_MAX*sizeof(int));

    //------------------------------------------------------------------------------

    //-----------------------УСТАНОВКА ПАРАМЕТРОВ ФИЛЬТРА---------------------------
    //Фильтр коррекции высоты РТП
    float rtp_height = 0.77;
    //Коррекция местоположения лазерного сканера на платформе
    float initX = 0;
    float initY = 0.27;
    //Фильтр по удалению точек РТП
    float rtp_forward = 0.5;
    float rtp_backward = 0.5;
    float rtp_side = 0.2;
    //Фильтр по уровневому удалению точек в вертикальной и горизонтальной плоскости
    float lowLevel = 0.3;
    float hightLevel = 1.2;
    //Фильтр по дальности
    float maxRange = 30;
    //-------------------------------------------------------------------------------

    //----------------------ИНИЦИАЛИЗАЦИЯ ПАРАМЕТРОВ---------------------------------
    //Разрешаем обработку
    isAllowToProcess = true;
    //Устанавливаем неготовность предыдущего облака точек
    isPreviousCloudReady = false;
    //Частота работы
    ros::Rate rate(10);
    numberOfReceivedCloud = 0;
    numberOfProceedClouds = 0;
    isReadytoNewCloud = true;
    //-------------------------------------------------------------------------------

    //----------------------ПАРАМЕТРЫ ТРАНСФОРМАЦИИ И ТРАЕКТОРИИ---------------------
    //Начальное вращение
    Eigen::VectorXf Qut(4);
    //Обозначаем углы
    float fi = 0;
    float teta = 0;
    float psi = 0;

    //Выполняем преобразование из углов в кватернион
    Qut(0) = cos(fi/2)*cos(teta/2)*cos(psi/2) + sin(fi/2) * sin (teta/2) * sin(psi/2);
    Qut(1) = sin(fi/2) * cos(teta/2) * cos(psi/2) - cos(fi/2) * sin(teta/2) * sin (psi/2);
    Qut(2) = cos(fi/2) * sin(teta/2) * cos(psi/2) + sin(fi/2) * cos(teta/2) * sin(psi/2);
    Qut(3) = cos(fi/2) * cos(teta/2) * sin(psi/2) - sin(fi/2) * sin(teta/2) * cos(psi/2);

    //Формируем кватернион
    Eigen::Quaternionf quat(Qut(0), Qut(1), Qut(2), Qut(3));

    //Отправляем его в массив
    Eigen::Quaternionf GlobalCoordinatesRotation = quat;

    //Начальное перемещение
    float GlobalCoordinatesTransl_x = 0;
    float GlobalCoordinatesTransl_y = 0;
    float GlobalCoordinatesTransl_z = 0;
    //-------------------------------------------------------------------------------


    //Сообщение о запуске
    ROS_INFO("ARCADA ICP started");

    //Функция при завершении работы
    signal(SIGINT, mySigintHandler);

    ROS_INFO("Start");


    while(ros::ok() && isAllowToProcess) {

        ROS_INFO("Start");
        ROS_INFO("TEST 20");

        cout<<"Number of received clouds "<<numberOfReceivedCloud<<std::endl;

        //Выполняем обработку
        if(isAllowToProcess && isReceivedCloud) {
            isProcessingPerforming = true;
            isAllowToProcess = false;

            ROS_INFO("Processing cloud...");

            filterCloud_CUDA(dev_pointCloud,
                             host_size,
                             dev_size,
                             dev_intensity,
                             rtp_height,
                             rtp_forward,
                             rtp_backward,
                             rtp_side,
                             lowLevel,
                             hightLevel,
                             maxRange,
                             initX,
                             initY);

            //Копируем обратно на память хоста
            cudaMemcpy(host_pointCloud,dev_pointCloud, *host_size * POINTS_DIM * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(host_intensity,dev_intensity, *host_size * sizeof(int), cudaMemcpyDeviceToHost);

            //Смотрим, сколько облаков получено

//            if(numberOfReceivedCloud > 1) {
                //Проверяем, готово ли предыдущее облако точек
                if(isPreviousCloudReady) {

                    //Время обработки
                    boost::posix_time::ptime startICP = boost::posix_time::microsec_clock::local_time();

                    //Если готово, то выполняем сопоставление
                    kernelICP(dev_pointCloudPrev,
                              dev_pointCloud,
                              dev_sizePrev,
                              dev_size,
                              host_sizePrev,
                              host_size,
                              dev_npIndex,
                              dev_mean_1,
                              dev_mean_2,
                              dev_M2,
                              dev_R,
                              iteration_ICP,
                              p1_seg,
                              dim_x,
                              dim_y,
                              dim_z,
                              segmentSize,
                              blocksPerGrid,
                              dev_NPnumber,
                              finalTransformation,
                              dev_finalTransformation,
                              QfinalTransf,
                              dev_mindist,
                              dev_intensity,
                              dev_intensityPrev);


                    boost::posix_time::ptime finishICP = boost::posix_time::microsec_clock::local_time();
                    boost::posix_time::time_duration durationICP( finishICP.time_of_day() - startICP.time_of_day());

                    std::cout<<"Duration of ICP "<<durationICP.total_microseconds()<<std::endl;

                    //Увеличиваем количество обработанных облаков
                    numberOfProceedClouds++;

                    //Проверяем успешность сопоставления
                    cout<<"Transformation "<<finalTransformation[0]<<" "<<finalTransformation[1]<<" "<<finalTransformation[2]<<std::endl;
                    if ( !(finalTransformation[0] != finalTransformation[0] || finalTransformation[1] != finalTransformation[1] || finalTransformation[2] != finalTransformation[2] ) /*&&
                         std::abs(finalTransformation[0]) < 0.1 && std::abs(finalTransformation[1]) < 0.2 && std::abs(finalTransformation[2]) < 0.01*/ /*&& std::abs(finalTransformation[3]) < 0.01 &&
                         std::abs(finalTransformation[4]) < 0.01 && std::abs(finalTransformation[5]) < 0.01*/) {
                        isAlignementSuccesfull = true;

                        //Добавляем координаты в массив преобразований
                        //Сначала вычисляем глобальное преобразование
                        //Вычисляем общее вращение
                        GlobalCoordinatesRotation = GlobalCoordinatesRotation * (*QfinalTransf);
                        //Из кватерниона вычисляем углы Эйлера
                        //Матрица поворота
                        Eigen::Matrix3f R_t;
                        GlobalCoordinatesRotation.normalize();
                        //Преобразуем кватернион в матрицу поворота
                        R_t = GlobalCoordinatesRotation.toRotationMatrix();
                        //И вытаскиваем оттсуда углы поворота
                        Eigen::Vector3f rotationAngles = R_t.eulerAngles(0,1,2);
                        float angle_x = rotationAngles(0);
                        float angle_y = rotationAngles(1);
                        float angle_z = rotationAngles(2);
                        //Находим глобальные координаты
                        GlobalCoordinatesTransl_x = GlobalCoordinatesTransl_x - finalTransformation[2] * sin(angle_y) + finalTransformation[0] * cos(angle_z)*cos(angle_y) - finalTransformation[1] * cos(angle_y)*sin(angle_z);
                        GlobalCoordinatesTransl_y = GlobalCoordinatesTransl_y
                                + finalTransformation[1] * (cos(angle_x)*cos(angle_z) - sin(angle_x)*sin(angle_y)*sin(angle_z)) + finalTransformation[0] * (cos(angle_x) * sin(angle_z) + cos(angle_z) * sin(angle_x)*sin(angle_y)) + finalTransformation[2] * cos(angle_y)*sin(angle_x);
                        GlobalCoordinatesTransl_z = GlobalCoordinatesTransl_z
                                + finalTransformation[0]*(sin(angle_x)*sin(angle_z) - cos(angle_x)*cos(angle_z)*sin(angle_y)) - finalTransformation[1]*(cos(angle_z)*sin(angle_x) + cos(angle_x)*sin(angle_z)*sin(angle_y)) + finalTransformation[2] * cos(angle_x)*cos(angle_y);

                        geometry_msgs::PoseStamped position;

                        SlamTraj.header.frame_id = "arcada_SLAM";

                        //Заполняем положение
                        position.pose.position.x = -GlobalCoordinatesTransl_x;
                        position.pose.position.y = -GlobalCoordinatesTransl_y;
                        position.pose.position.z = GlobalCoordinatesTransl_z;

                        position.pose.orientation.x = GlobalCoordinatesRotation.x();
                        position.pose.orientation.y = GlobalCoordinatesRotation.y();
                        position.pose.orientation.z = GlobalCoordinatesRotation.z();
                        position.pose.orientation.w = GlobalCoordinatesRotation.w();



                        //Добавляем точку
                        SlamTraj.poses.push_back(position);

                        ROS_INFO("Publish translation");

                        SlamTraj.header.stamp = ros::Time::now();
                        TrajPub.publish(SlamTraj);





                        //Возвращаем в ложь готовность облака точек
                        isPreviousCloudReady = false;

                    }
                    else {
                        //устанавливаем флаг успешности сопоставление в ложь
//                        isAlignementSuccesfull = false;
                    }
                    isReadytoNewCloud = true;
                }
                else {
                    isReadytoNewCloud = true;
                }
                std::cout<<" Current transformation "<<finalTransformation[0]<<" "<<finalTransformation[1]<<" "<<finalTransformation[2];
//            }

                //Публикуем трансформацию
                tf::tfMessage TFMEs;
                geometry_msgs::TransformStamped TS;

                geometry_msgs::Quaternion Qls;


                float roll = M_PI;
                float pitch = 0;
                float yaw = M_PI/2;


                Qls.x = cos(yaw/2) * cos(pitch/2) * cos(roll/2) + sin(yaw/2)*sin(pitch/2)*sin(roll/2);
                Qls.y = sin(yaw/2) * cos(pitch/2) * cos(roll/2) + cos(yaw/2)*sin(pitch/2)*sin(roll/2);
                Qls.z = cos(yaw/2) * sin(pitch/2) * cos(roll/2) + sin(yaw/2)*cos(pitch/2)*sin(roll/2);
                Qls.w = cos(yaw/2) * cos(pitch/2) * sin(roll/2) + sin(yaw/2)*sin(pitch/2)*cos(roll/2);

                TS.transform.translation.x = -0.27;
                TS.transform.translation.y = 0;
                TS.transform.translation.z = 0.58;
                TS.transform.rotation = Qls;

                TS.header.frame_id = "odom";
                TS.child_frame_id = "arcada_SLAM";
                TS.header.stamp = ros::Time::now();
                TFMEs.transforms.push_back(TS);
                transfPub.publish(TFMEs);
                //Публикуем облако точек 89514608991 sudo setcap cap_net_raw+ep RobotLaserSystem
                ROS_INFO("Publish point cloud");
                pointCloudMain.header.frame_id = "arcada_transformedPointCloud";
                pointCloudMain.header.stamp = ros::Time::now();
//                pubCloud.publish(pointCloudMain);


            //Устанавливаем флаги
            isAllowToProcess = true;
            isReceivedCloud = false;
            isProcessingPerforming = false;
        }

        ros::spinOnce();
        rate.sleep();
    }


    ROS_INFO("FILTER CLOUD finished");
    return 0;

}
