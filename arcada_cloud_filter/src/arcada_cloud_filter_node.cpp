#include <ros/ros.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include <fstream>
#include <sys/types.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <iomanip>

#include <cuda.h>
#include <cuda_runtime.h>

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include "sensor_msgs/ChannelFloat32.h"
#include "nav_msgs/OccupancyGrid.h"

#include "boost/date_time/posix_time/posix_time.hpp"

#define LASER_NMB 32
#define MAX_BLOCKS_PER_ROT 2500
#define RAD_PER_GRAD 0.0174532925
#define N_MAX 70000
#define POINT_DIM 3
#define POINTS_DIM 3

sensor_msgs::PointCloud pointCloudMain;

using namespace std;

//--------------------УКАЗАТЕЛИ НА МАССИВЫ ДАННЫХ------------
static float *dev_pointCloud;
static int *dev_size;
static int *dev_intensity;
static float *host_pointCloud;
static int *host_size;
static int *host_intensity;
//----------------------------------------------------------

//------------------------ФЛАГИ---------------------------------------------------------
//Флаг на разрешение обработки данных
static bool isAllowToProcess;
static bool isProcessingPerforming;
static bool isReceivedCloud;
//--------------------------------------------------------------------------------------

//Объявление функции анализа проходимости на видеокарте
void filterCloud_CUDA(float *dev_pointCloud,
                      int *host_p_size,
                      int *dev_p_size,
                      int *dev_intensity,
                      float rtp_height,
                      float rtp_forward,
                      float rtp_backward,
                      float rtp_side,
                      float lowLevel,
                      float hightLevel,
                      float maxRange);

//Функция принятия облака точек
void pointCloud_callback(const sensor_msgs::PointCloud &pointCloud) {

    if(isAllowToProcess) {

        isProcessingPerforming = true;
        ROS_INFO("Receive cloud");

        *host_size = pointCloud.points.size();
        //Переносим все параметры
        for(int i = 0; i < pointCloud.points.size(); i++) {
            host_pointCloud[POINT_DIM * i + 0] = pointCloud.points[i].x;
            host_pointCloud[POINT_DIM * i + 1] = pointCloud.points[i].y;
            host_pointCloud[POINT_DIM * i + 2] = pointCloud.points[i].z;
            //Интенсивность
            host_intensity[i] = pointCloud.channels[0].values[i];
        }
        //Копируем на память устройства
        cudaMemcpy(dev_pointCloud, host_pointCloud, *host_size * POINTS_DIM * sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(dev_intensity, host_intensity, *host_size * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dev_size, host_size, sizeof(int), cudaMemcpyHostToDevice);
        isReceivedCloud = true;
        isProcessingPerforming = false;
    }


}

//Функция окончания работы
void mySigintHandler(int sig) {

    //Ждём, пока не снимется флаг с обработки
    while(isProcessingPerforming) {
       usleep(1);
    }
    isAllowToProcess = false;

    //Освобождаем память
    cudaFree(dev_pointCloud);
    cudaFree(dev_size);
    cudaFree(dev_intensity);

    free(host_pointCloud);
    free(host_intensity);
    free(host_size);


    ROS_INFO("CLOUD FILTER stopped");

  ros::shutdown();
}



int main(int argc, char **argv) {

    //Начальная инициализация узла
    ros::init(argc, argv, "arcada_cloud_filter_node");
    ros::NodeHandle n;
    std::string frame_id;
    n.param<std::string> ("frame_id", frame_id, "/arcada_pointCloud");

    //-----------------------УКАЗАТЕЛИ НА ПЕРЕСЫЛАЕМЫЕ И ПРИНИМАЕМЫЕ ДАННЫЕ----------
    nav_msgs::OccupancyGrid OccupancyMap;
    //Подписчик на облако точек
    ros::Subscriber pointCloud_sub = n.subscribe("arcada/velodyne_pointCloud",32, pointCloud_callback);
    //Публикатор карты проходимости
    ros::Publisher pointCloudFilteredPub = n.advertise<sensor_msgs::PointCloud>("arcada/filtered_cloud",32);
    //-------------------------------------------------------------------------------

    //-----------------------ВЫДЕЛЕНИЕ ПАМЯТИ ПОД МАССИВЫ ДАННЫХ--------------------
    cudaMalloc((void**)&dev_pointCloud, POINT_DIM*N_MAX*sizeof(float));
    cudaMalloc((void**)&dev_intensity, N_MAX*sizeof(int));
    cudaMalloc((void**)&dev_size, sizeof(int));

    host_pointCloud = (float *) malloc(N_MAX * POINTS_DIM * sizeof(float));
    host_intensity = (int *) malloc(N_MAX * sizeof(int));
    host_size = (int *) malloc(sizeof(int));
    //------------------------------------------------------------------------------

    //-----------------------УСТАНОВКА ПАРАМЕТРОВ ФИЛЬТРА---------------------------
    //Фильтр коррекции высоты РТП
    float rtp_height = 0.55;
    //Фильтр по удалению точек РТП
    float rtp_forward = 0.5;
    float rtp_backward = 0.5;
    float rtp_side = 0.2;
    //Фильтр по уровневому удалению точек в вертикальной и горизонтальной плоскости
    float lowLevel = 0.2;
    float hightLevel = 2.5;
    //Фильтр по дальности
    float maxRange = 30;
    //-------------------------------------------------------------------------------


    //Сообщение о запуске
    ROS_INFO("CLOUD FILTER started");

    //Функция при завершении работы
    signal(SIGINT, mySigintHandler);

    //Частота работы
    ros::Rate rate(10); //Hz

    //Разрешаем обработку
    isAllowToProcess = true;

    while(ros::ok()) {

        //Выполняем обработку
        if(isAllowToProcess && isReceivedCloud) {
            isProcessingPerforming = true;
            isAllowToProcess = false;

            filterCloud_CUDA(dev_pointCloud,
                             host_size,
                             dev_size,
                             dev_intensity,
                             rtp_height,
                             rtp_forward,
                             rtp_backward,
                             rtp_side,
                             lowLevel,
                             hightLevel,
                             maxRange);

            //Копируем обратно на память хоста
            cudaMemcpy(host_pointCloud,dev_pointCloud, *host_size * POINTS_DIM * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(host_intensity,dev_intensity, *host_size * sizeof(int), cudaMemcpyDeviceToHost);


            //Формируем облако точек
            //Очищаем облако точек
            pointCloudMain.points.clear();
            pointCloudMain.channels.clear();
            pointCloudMain.channels.resize(1);
            pointCloudMain.channels[0].name = "intensity";
            pointCloudMain.channels[0].values.resize(*host_size);

            //Проходимся по всем пакетам и записываем их в очерёдности
            for(int i = 0; i< *host_size; i++) {
                    geometry_msgs::Point32 point;
                    point.x = host_pointCloud[i*POINTS_DIM + 0];
                    point.y = host_pointCloud[i*POINTS_DIM + 1];
                    point.z = host_pointCloud[i*POINTS_DIM+ 2];
                    //Выводим точку в облако
                    pointCloudMain.points.push_back(point);

                    //Интенсивность отражения точки
                    pointCloudMain.channels[0].values[i] = host_intensity[i];

            }
            //Публикуем облако точек
            ROS_INFO("Publish map");
            pointCloudMain.header.frame_id = frame_id;
            pointCloudMain.header.stamp = ros::Time::now();
            pointCloudFilteredPub.publish(pointCloudMain);


            //Устанавливаем флаги
            isAllowToProcess = true;
            isReceivedCloud = false;
            isProcessingPerforming = false;
        }

        ros::spinOnce();
        rate.sleep();
    }


    ROS_INFO("FILTER CLOUD finished");
    return 0;

}
